import 'package:flutter/widgets.dart';

class HandmadeIcons {
  HandmadeIcons._();

  static const String _fontFamily = 'handmade';

  /// zoom_out Icon
  static const IconData zoom_out =
      IconData(0xe900, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zoom_in Icon
  static const IconData zoom_in =
      IconData(0xe901, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wrench Icon
  static const IconData wrench =
      IconData(0xe902, fontFamily: _fontFamily, fontPackage: "mbi");

  /// user_1 Icon
  static const IconData user_1 =
      IconData(0xe903, fontFamily: _fontFamily, fontPackage: "mbi");

  /// user Icon
  static const IconData user =
      IconData(0xe904, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upload_3 Icon
  static const IconData upload_3 =
      IconData(0xe905, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upload_2 Icon
  static const IconData upload_2 =
      IconData(0xe906, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upload_1 Icon
  static const IconData upload_1 =
      IconData(0xe907, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upload Icon
  static const IconData upload =
      IconData(0xe908, fontFamily: _fontFamily, fontPackage: "mbi");

  /// up_arrow_2 Icon
  static const IconData up_arrow_2 =
      IconData(0xe909, fontFamily: _fontFamily, fontPackage: "mbi");

  /// up_arrow_1 Icon
  static const IconData up_arrow_1 =
      IconData(0xe90a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// up_arrow Icon
  static const IconData up_arrow =
      IconData(0xe90b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// transfer_2 Icon
  static const IconData transfer_2 =
      IconData(0xe90c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// transfer_1 Icon
  static const IconData transfer_1 =
      IconData(0xe90d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// transfer Icon
  static const IconData transfer =
      IconData(0xe90e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// telephone_1 Icon
  static const IconData telephone_1 =
      IconData(0xe90f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// telephone Icon
  static const IconData telephone =
      IconData(0xe910, fontFamily: _fontFamily, fontPackage: "mbi");

  /// target Icon
  static const IconData target =
      IconData(0xe911, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sticker Icon
  static const IconData sticker =
      IconData(0xe912, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble_3 Icon
  static const IconData speech_bubble_3 =
      IconData(0xe913, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble_2 Icon
  static const IconData speech_bubble_2 =
      IconData(0xe914, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble_1 Icon
  static const IconData speech_bubble_1 =
      IconData(0xe915, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble Icon
  static const IconData speech_bubble =
      IconData(0xe916, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speaker Icon
  static const IconData speaker =
      IconData(0xe917, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sort Icon
  static const IconData sort =
      IconData(0xe918, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shuffle_1 Icon
  static const IconData shuffle_1 =
      IconData(0xe919, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shuffle Icon
  static const IconData shuffle =
      IconData(0xe91a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shield Icon
  static const IconData shield =
      IconData(0xe91b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// share Icon
  static const IconData share =
      IconData(0xe91c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// settings Icon
  static const IconData settings =
      IconData(0xe91d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// send Icon
  static const IconData send =
      IconData(0xe91e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// search Icon
  static const IconData search =
      IconData(0xe91f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// right_arrow_1 Icon
  static const IconData right_arrow_1 =
      IconData(0xe920, fontFamily: _fontFamily, fontPackage: "mbi");

  /// right_arrow Icon
  static const IconData right_arrow =
      IconData(0xe921, fontFamily: _fontFamily, fontPackage: "mbi");

  /// repeat_3 Icon
  static const IconData repeat_3 =
      IconData(0xe922, fontFamily: _fontFamily, fontPackage: "mbi");

  /// repeat_2 Icon
  static const IconData repeat_2 =
      IconData(0xe923, fontFamily: _fontFamily, fontPackage: "mbi");

  /// repeat_1 Icon
  static const IconData repeat_1 =
      IconData(0xe924, fontFamily: _fontFamily, fontPackage: "mbi");

  /// repeat Icon
  static const IconData repeat =
      IconData(0xe925, fontFamily: _fontFamily, fontPackage: "mbi");

  /// reload Icon
  static const IconData reload =
      IconData(0xe926, fontFamily: _fontFamily, fontPackage: "mbi");

  /// refresh Icon
  static const IconData refresh =
      IconData(0xe927, fontFamily: _fontFamily, fontPackage: "mbi");

  /// push_pin Icon
  static const IconData push_pin =
      IconData(0xe928, fontFamily: _fontFamily, fontPackage: "mbi");

  /// previous_2 Icon
  static const IconData previous_2 =
      IconData(0xe929, fontFamily: _fontFamily, fontPackage: "mbi");

  /// previous_1 Icon
  static const IconData previous_1 =
      IconData(0xe92a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// previous Icon
  static const IconData previous =
      IconData(0xe92b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// paper_clip Icon
  static const IconData paper_clip =
      IconData(0xe92c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// padlock_1 Icon
  static const IconData padlock_1 =
      IconData(0xe92d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// padlock Icon
  static const IconData padlock =
      IconData(0xe92e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// next_3 Icon
  static const IconData next_3 =
      IconData(0xe92f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// next_2 Icon
  static const IconData next_2 =
      IconData(0xe930, fontFamily: _fontFamily, fontPackage: "mbi");

  /// next_1 Icon
  static const IconData next_1 =
      IconData(0xe931, fontFamily: _fontFamily, fontPackage: "mbi");

  /// next Icon
  static const IconData next =
      IconData(0xe932, fontFamily: _fontFamily, fontPackage: "mbi");

  /// muted Icon
  static const IconData muted =
      IconData(0xe933, fontFamily: _fontFamily, fontPackage: "mbi");

  /// musical_note Icon
  static const IconData musical_note =
      IconData(0xe934, fontFamily: _fontFamily, fontPackage: "mbi");

  /// move Icon
  static const IconData move =
      IconData(0xe935, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mail Icon
  static const IconData mail =
      IconData(0xe936, fontFamily: _fontFamily, fontPackage: "mbi");

  /// login_1 Icon
  static const IconData login_1 =
      IconData(0xe937, fontFamily: _fontFamily, fontPackage: "mbi");

  /// login Icon
  static const IconData login =
      IconData(0xe938, fontFamily: _fontFamily, fontPackage: "mbi");

  /// list Icon
  static const IconData list =
      IconData(0xe939, fontFamily: _fontFamily, fontPackage: "mbi");

  /// link Icon
  static const IconData link =
      IconData(0xe93a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// levels_1 Icon
  static const IconData levels_1 =
      IconData(0xe93b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// levels Icon
  static const IconData levels =
      IconData(0xe93c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// left_arrow_2 Icon
  static const IconData left_arrow_2 =
      IconData(0xe93d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// left_arrow_1 Icon
  static const IconData left_arrow_1 =
      IconData(0xe93e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// left_arrow Icon
  static const IconData left_arrow =
      IconData(0xe93f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// inbox Icon
  static const IconData inbox =
      IconData(0xe940, fontFamily: _fontFamily, fontPackage: "mbi");

  /// home Icon
  static const IconData home =
      IconData(0xe941, fontFamily: _fontFamily, fontPackage: "mbi");

  /// headphones Icon
  static const IconData headphones =
      IconData(0xe942, fontFamily: _fontFamily, fontPackage: "mbi");

  /// head Icon
  static const IconData head =
      IconData(0xe943, fontFamily: _fontFamily, fontPackage: "mbi");

  /// forbidden Icon
  static const IconData forbidden =
      IconData(0xe944, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder Icon
  static const IconData folder =
      IconData(0xe945, fontFamily: _fontFamily, fontPackage: "mbi");

  /// export Icon
  static const IconData export =
      IconData(0xe946, fontFamily: _fontFamily, fontPackage: "mbi");

  /// expand Icon
  static const IconData expand =
      IconData(0xe947, fontFamily: _fontFamily, fontPackage: "mbi");

  /// edit Icon
  static const IconData edit =
      IconData(0xe948, fontFamily: _fontFamily, fontPackage: "mbi");

  /// download_4 Icon
  static const IconData download_4 =
      IconData(0xe949, fontFamily: _fontFamily, fontPackage: "mbi");

  /// download_3 Icon
  static const IconData download_3 =
      IconData(0xe94a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// download_2 Icon
  static const IconData download_2 =
      IconData(0xe94b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// download_1 Icon
  static const IconData download_1 =
      IconData(0xe94c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// download Icon
  static const IconData download =
      IconData(0xe94d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// down_arrow_1 Icon
  static const IconData down_arrow_1 =
      IconData(0xe94e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// down_arrow Icon
  static const IconData down_arrow =
      IconData(0xe94f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cursor_2 Icon
  static const IconData cursor_2 =
      IconData(0xe950, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cursor_1 Icon
  static const IconData cursor_1 =
      IconData(0xe951, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cursor Icon
  static const IconData cursor =
      IconData(0xe952, fontFamily: _fontFamily, fontPackage: "mbi");

  /// compress Icon
  static const IconData compress =
      IconData(0xe953, fontFamily: _fontFamily, fontPackage: "mbi");

  /// coins Icon
  static const IconData coins =
      IconData(0xe954, fontFamily: _fontFamily, fontPackage: "mbi");

  /// coin Icon
  static const IconData coin =
      IconData(0xe955, fontFamily: _fontFamily, fontPackage: "mbi");

  /// close_1 Icon
  static const IconData close_1 =
      IconData(0xe956, fontFamily: _fontFamily, fontPackage: "mbi");

  /// close Icon
  static const IconData close =
      IconData(0xe957, fontFamily: _fontFamily, fontPackage: "mbi");

  /// click Icon
  static const IconData click =
      IconData(0xe958, fontFamily: _fontFamily, fontPackage: "mbi");

  /// checked_2 Icon
  static const IconData checked_2 =
      IconData(0xe959, fontFamily: _fontFamily, fontPackage: "mbi");

  /// checked_1 Icon
  static const IconData checked_1 =
      IconData(0xe95a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// checked Icon
  static const IconData checked =
      IconData(0xe95b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// broken_link Icon
  static const IconData broken_link =
      IconData(0xe95c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bookmark Icon
  static const IconData bookmark =
      IconData(0xe95d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// basket Icon
  static const IconData basket =
      IconData(0xe95e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// add Icon
  static const IconData add =
      IconData(0xe95f, fontFamily: _fontFamily, fontPackage: "mbi");
}
