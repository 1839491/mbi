import 'package:flutter/widgets.dart';

/// 49 Emoji Face icon
class EmojiIcons {
  EmojiIcons._();

  /// The icon font family name
  static const String _fontFamily = 'emoji';

  ///angry icon
  static const IconData angry =
      IconData(0xe900, fontFamily: _fontFamily, fontPackage: "mbi");

  ///angry_1 icon
  static const IconData angry_1 =
      IconData(0xe901, fontFamily: _fontFamily, fontPackage: "mbi");

  ///angry_2 icon
  static const IconData angry_2 =
      IconData(0xe902, fontFamily: _fontFamily, fontPackage: "mbi");

  ///bored icon
  static const IconData bored =
      IconData(0xe903, fontFamily: _fontFamily, fontPackage: "mbi");

  ///bored_1 icon
  static const IconData bored_1 =
      IconData(0xe904, fontFamily: _fontFamily, fontPackage: "mbi");

  ///crying icon
  static const IconData crying =
      IconData(0xe905, fontFamily: _fontFamily, fontPackage: "mbi");

  ///dead icon
  static const IconData dead =
      IconData(0xe906, fontFamily: _fontFamily, fontPackage: "mbi");

  ///dead_1 icon
  static const IconData dead_1 =
      IconData(0xe907, fontFamily: _fontFamily, fontPackage: "mbi");

  ///giggle icon
  static const IconData giggle =
      IconData(0xe908, fontFamily: _fontFamily, fontPackage: "mbi");

  ///goofy icon
  static const IconData goofy =
      IconData(0xe909, fontFamily: _fontFamily, fontPackage: "mbi");

  ///goofy_1 icon
  static const IconData goofy_1 =
      IconData(0xe90a, fontFamily: _fontFamily, fontPackage: "mbi");

  ///goofy_2 icon
  static const IconData goofy_2 =
      IconData(0xe90b, fontFamily: _fontFamily, fontPackage: "mbi");

  ///goofy_3 icon
  static const IconData goofy_3 =
      IconData(0xe90c, fontFamily: _fontFamily, fontPackage: "mbi");

  ///happy icon
  static const IconData happy =
      IconData(0xe90d, fontFamily: _fontFamily, fontPackage: "mbi");

  ///happy_1 icon
  static const IconData happy_1 =
      IconData(0xe90e, fontFamily: _fontFamily, fontPackage: "mbi");

  ///happy_2 icon
  static const IconData happy_2 =
      IconData(0xe90f, fontFamily: _fontFamily, fontPackage: "mbi");

  ///happy_3 icon
  static const IconData happy_3 =
      IconData(0xe910, fontFamily: _fontFamily, fontPackage: "mbi");

  ///happy_4 icon
  static const IconData happy_4 =
      IconData(0xe911, fontFamily: _fontFamily, fontPackage: "mbi");

  ///happy_5 icon
  static const IconData happy_5 =
      IconData(0xe912, fontFamily: _fontFamily, fontPackage: "mbi");

  ///happy_6 icon
  static const IconData happy_6 =
      IconData(0xe913, fontFamily: _fontFamily, fontPackage: "mbi");

  ///looking icon
  static const IconData looking =
      IconData(0xe914, fontFamily: _fontFamily, fontPackage: "mbi");

  ///looking_1 icon
  static const IconData looking_1 =
      IconData(0xe915, fontFamily: _fontFamily, fontPackage: "mbi");

  ///looking_2 icon
  static const IconData looking_2 =
      IconData(0xe916, fontFamily: _fontFamily, fontPackage: "mbi");

  ///nerd icon
  static const IconData nerd =
      IconData(0xe917, fontFamily: _fontFamily, fontPackage: "mbi");

  ///pirate icon
  static const IconData pirate =
      IconData(0xe918, fontFamily: _fontFamily, fontPackage: "mbi");

  ///pirate_1 icon
  static const IconData pirate_1 =
      IconData(0xe919, fontFamily: _fontFamily, fontPackage: "mbi");

  ///pirate_2 icon
  static const IconData pirate_2 =
      IconData(0xe91a, fontFamily: _fontFamily, fontPackage: "mbi");

  ///pirate_3 icon
  static const IconData pirate_3 =
      IconData(0xe91b, fontFamily: _fontFamily, fontPackage: "mbi");

  ///pirate_4 icon
  static const IconData pirate_4 =
      IconData(0xe91c, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad icon
  static const IconData sad =
      IconData(0xe91d, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_1 icon
  static const IconData sad_1 =
      IconData(0xe91e, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_2 icon
  static const IconData sad_2 =
      IconData(0xe91f, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_3 icon
  static const IconData sad_3 =
      IconData(0xe920, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_4 icon
  static const IconData sad_4 =
      IconData(0xe921, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_5 icon
  static const IconData sad_5 =
      IconData(0xe922, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_6 icon
  static const IconData sad_6 =
      IconData(0xe923, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_7 icon
  static const IconData sad_7 =
      IconData(0xe924, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_8 icon
  static const IconData sad_8 =
      IconData(0xe925, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_9 icon
  static const IconData sad_9 =
      IconData(0xe926, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_10 icon
  static const IconData sad_10 =
      IconData(0xe927, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_11 icon
  static const IconData sad_11 =
      IconData(0xe928, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_12 icon
  static const IconData sad_12 =
      IconData(0xe929, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_13 icon
  static const IconData sad_13 =
      IconData(0xe92a, fontFamily: _fontFamily, fontPackage: "mbi");

  ///sad_14 icon
  static const IconData sad_14 =
      IconData(0xe92b, fontFamily: _fontFamily, fontPackage: "mbi");

  ///shocked icon
  static const IconData shocked =
      IconData(0xe92c, fontFamily: _fontFamily, fontPackage: "mbi");

  ///shocked_1 icon
  static const IconData shocked_1 =
      IconData(0xe92d, fontFamily: _fontFamily, fontPackage: "mbi");

  ///shocked_2 icon
  static const IconData shocked_2 =
      IconData(0xe92e, fontFamily: _fontFamily, fontPackage: "mbi");

  ///shocked_3 icon
  static const IconData shocked_3 =
      IconData(0xe92f, fontFamily: _fontFamily, fontPackage: "mbi");

  ///thinking icon
  static const IconData thinking =
      IconData(0xe930, fontFamily: _fontFamily, fontPackage: "mbi");

  ///wink icon
  static const IconData wink =
      IconData(0xe931, fontFamily: _fontFamily, fontPackage: "mbi");
}
