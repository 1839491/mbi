import 'package:flutter/widgets.dart';

/// 49 file&folders types icon
class FileIcons {
  FileIcons._();

  /// The icon font family name
  static const String _fontFamily = 'file';

  /// file35 icon
  static const IconData file35 =
      IconData(0xe900, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file34 icon
  static const IconData file34 =
      IconData(0xe901, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file33 icon
  static const IconData file33 =
      IconData(0xe902, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file32 icon
  static const IconData file32 =
      IconData(0xe903, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file31 icon
  static const IconData file31 =
      IconData(0xe904, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file30 icon
  static const IconData file30 =
      IconData(0xe905, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file29 icon
  static const IconData file29 =
      IconData(0xe906, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file28 icon
  static const IconData file28 =
      IconData(0xe907, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file27 icon
  static const IconData file27 =
      IconData(0xe908, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file26 icon
  static const IconData file26 =
      IconData(0xe909, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file25 icon
  static const IconData file25 =
      IconData(0xe90a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file24 icon
  static const IconData file24 =
      IconData(0xe90b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file23 icon
  static const IconData file23 =
      IconData(0xe90c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder4 icon
  static const IconData folder4 =
      IconData(0xe90d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder5 icon
  static const IconData folder5 =
      IconData(0xe90e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder6 icon
  static const IconData folder6 =
      IconData(0xe90f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder7 icon
  static const IconData folder7 =
      IconData(0xe910, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder8 icon
  static const IconData folder8 =
      IconData(0xe911, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder3 icon
  static const IconData folder3 =
      IconData(0xe912, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder2 icon
  static const IconData folder2 =
      IconData(0xe913, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder1 icon
  static const IconData folder1 =
      IconData(0xe914, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file22 icon
  static const IconData file22 =
      IconData(0xe915, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder icon
  static const IconData folder =
      IconData(0xe916, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file21 icon
  static const IconData file21 =
      IconData(0xe917, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file20 icon
  static const IconData file20 =
      IconData(0xe918, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file19 icon
  static const IconData file19 =
      IconData(0xe919, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file18 icon
  static const IconData file18 =
      IconData(0xe91a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file17 icon
  static const IconData file17 =
      IconData(0xe91b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file16 icon
  static const IconData file16 =
      IconData(0xe91c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file15 icon
  static const IconData file15 =
      IconData(0xe91d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file14 icon
  static const IconData file14 =
      IconData(0xe91e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file13 icon
  static const IconData file13 =
      IconData(0xe91f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file36 icon
  static const IconData file36 =
      IconData(0xe920, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file37 icon
  static const IconData file37 =
      IconData(0xe921, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file38 icon
  static const IconData file38 =
      IconData(0xe922, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file39 icon
  static const IconData file39 =
      IconData(0xe923, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file12 icon
  static const IconData file12 =
      IconData(0xe924, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file11 icon
  static const IconData file11 =
      IconData(0xe925, fontFamily: _fontFamily, fontPackage: "mbi");

  /// garbage icon
  static const IconData garbage =
      IconData(0xe926, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file10 icon
  static const IconData file10 =
      IconData(0xe927, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file9 icon
  static const IconData file9 =
      IconData(0xe928, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file8 icon
  static const IconData file8 =
      IconData(0xe929, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file7 icon
  static const IconData file7 =
      IconData(0xe92a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file6 icon
  static const IconData file6 =
      IconData(0xe92b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file5 icon
  static const IconData file5 =
      IconData(0xe92c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file4 icon
  static const IconData file4 =
      IconData(0xe92d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file3 icon
  static const IconData file3 =
      IconData(0xe92e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file2 icon
  static const IconData file2 =
      IconData(0xe92f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file1 icon
  static const IconData file1 =
      IconData(0xe930, fontFamily: _fontFamily, fontPackage: "mbi");

  /// file icon
  static const IconData file =
      IconData(0xe931, fontFamily: _fontFamily, fontPackage: "mbi");
}
