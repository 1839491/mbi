import 'package:flutter/widgets.dart';

/// 47 Beautiful LineCons Icons
class LineconsIcons {
  LineconsIcons._();

  /// The icon font family name
  static const String _fontFamily = 'linecons';

  /// heart1 icon
  static const IconData heart1 =
      IconData(0xe900, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cloud1 icon
  static const IconData cloud1 =
      IconData(0xe901, fontFamily: _fontFamily, fontPackage: "mbi");

  /// star icon
  static const IconData star =
      IconData(0xe902, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tv1 icon
  static const IconData tv1 =
      IconData(0xe903, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sound icon
  static const IconData sound =
      IconData(0xe904, fontFamily: _fontFamily, fontPackage: "mbi");

  /// video icon
  static const IconData video =
      IconData(0xe905, fontFamily: _fontFamily, fontPackage: "mbi");

  /// trash icon
  static const IconData trash =
      IconData(0xe906, fontFamily: _fontFamily, fontPackage: "mbi");

  /// user1 icon
  static const IconData user1 =
      IconData(0xe907, fontFamily: _fontFamily, fontPackage: "mbi");

  /// key1 icon
  static const IconData key1 =
      IconData(0xe908, fontFamily: _fontFamily, fontPackage: "mbi");

  /// search1 icon
  static const IconData search1 =
      IconData(0xe909, fontFamily: _fontFamily, fontPackage: "mbi");

  /// settings icon
  static const IconData settings =
      IconData(0xe90a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// camera1 icon
  static const IconData camera1 =
      IconData(0xe90b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tag icon
  static const IconData tag =
      IconData(0xe90c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// lock1 icon
  static const IconData lock1 =
      IconData(0xe90d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bulb icon
  static const IconData bulb =
      IconData(0xe90e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pen1 icon
  static const IconData pen1 =
      IconData(0xe90f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// diamond icon
  static const IconData diamond =
      IconData(0xe910, fontFamily: _fontFamily, fontPackage: "mbi");

  /// display1 icon
  static const IconData display1 =
      IconData(0xe911, fontFamily: _fontFamily, fontPackage: "mbi");

  /// location1 icon
  static const IconData location1 =
      IconData(0xe912, fontFamily: _fontFamily, fontPackage: "mbi");

  /// eye1 icon
  static const IconData eye1 =
      IconData(0xe913, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bubble1 icon
  static const IconData bubble1 =
      IconData(0xe914, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stack1 icon
  static const IconData stack1 =
      IconData(0xe915, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cup icon
  static const IconData cup =
      IconData(0xe916, fontFamily: _fontFamily, fontPackage: "mbi");

  /// phone1 icon
  static const IconData phone1 =
      IconData(0xe917, fontFamily: _fontFamily, fontPackage: "mbi");

  /// news icon
  static const IconData news =
      IconData(0xe918, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mail1 icon
  static const IconData mail1 =
      IconData(0xe919, fontFamily: _fontFamily, fontPackage: "mbi");

  /// like icon
  static const IconData like =
      IconData(0xe91a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// photo icon
  static const IconData photo =
      IconData(0xe91b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// note icon
  static const IconData note =
      IconData(0xe91c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// clock1 icon
  static const IconData clock1 =
      IconData(0xe91d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// paperplane icon
  static const IconData paperplane =
      IconData(0xe91e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// params icon
  static const IconData params =
      IconData(0xe91f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// banknote icon
  static const IconData banknote =
      IconData(0xe920, fontFamily: _fontFamily, fontPackage: "mbi");

  /// data icon
  static const IconData data =
      IconData(0xe921, fontFamily: _fontFamily, fontPackage: "mbi");

  /// music1 icon
  static const IconData music1 =
      IconData(0xe922, fontFamily: _fontFamily, fontPackage: "mbi");

  /// megaphone icon
  static const IconData megaphone =
      IconData(0xe923, fontFamily: _fontFamily, fontPackage: "mbi");

  /// study icon
  static const IconData study =
      IconData(0xe924, fontFamily: _fontFamily, fontPackage: "mbi");

  /// lab1 icon
  static const IconData lab1 =
      IconData(0xe925, fontFamily: _fontFamily, fontPackage: "mbi");

  /// food icon
  static const IconData food =
      IconData(0xe926, fontFamily: _fontFamily, fontPackage: "mbi");

  /// t_shirt icon
  static const IconData t_shirt =
      IconData(0xe927, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fire1 icon
  static const IconData fire1 =
      IconData(0xe928, fontFamily: _fontFamily, fontPackage: "mbi");

  /// clip icon
  static const IconData clip =
      IconData(0xe929, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shop icon
  static const IconData shop =
      IconData(0xe92a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// calendar1 icon
  static const IconData calendar1 =
      IconData(0xe92b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wallet icon
  static const IconData wallet =
      IconData(0xe92c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vynil icon
  static const IconData vynil =
      IconData(0xe92d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// truck1 icon
  static const IconData truck1 =
      IconData(0xe92e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// world icon
  static const IconData world =
      IconData(0xe92f, fontFamily: _fontFamily, fontPackage: "mbi");
}
