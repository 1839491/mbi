import 'package:flutter/widgets.dart';

/// The Brands and companies logo and icons
class BrandsIcons {
  BrandsIcons._();

  /// The icon font family name
  static const String _fontFamily = 'brands';

  /// Password Icon
  static const IconData password =
      IconData(0xe96c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// PX Icon
  static const IconData px1 =
      IconData(0xe96d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Abbrobot Studio Icon
  static const IconData abbrobotstudio =
      IconData(0xe96e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// About.me Icon
  static const IconData about_dot_me =
      IconData(0xe96f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Abstract Icon
  static const IconData abstract =
      IconData(0xe970, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Academia Icon
  static const IconData academia =
      IconData(0xe971, fontFamily: _fontFamily, fontPackage: "mbi");

  /// AccuSoft Icon
  static const IconData accusoft =
      IconData(0xe972, fontFamily: _fontFamily, fontPackage: "mbi");

  /// acm Icon
  static const IconData acm =
      IconData(0xe973, fontFamily: _fontFamily, fontPackage: "mbi");

  /// add This Icon
  static const IconData addthis =
      IconData(0xe974, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Ad Guard Icon
  static const IconData adguard =
      IconData(0xe975, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Adobe Icon
  static const IconData adobe =
      IconData(0xe976, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Adobe Acrobat reader Icon
  static const IconData adobeacrobatreader =
      IconData(0xe977, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Adobe After Effects Icon
  static const IconData adobeaftereffects =
      IconData(0xe978, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Adobe Audition Icon
  static const IconData adobeaudition =
      IconData(0xe979, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Adobe Creative cloud Icon
  static const IconData adobecreativecloud =
      IconData(0xe97a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// Adobe Dreamweaver Icon
  /// adobedreamweaver icon
  static const IconData adobedreamweaver =
      IconData(0xe97b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// adobeillustrator icon
  static const IconData adobeillustrator =
      IconData(0xe97c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// adobeindesign icon
  static const IconData adobeindesign =
      IconData(0xe97d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// adobelightroomcc icon
  static const IconData adobelightroomcc =
      IconData(0xe97e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// adobelightroomclassic icon
  static const IconData adobelightroomclassic =
      IconData(0xe97f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// adobephotoshop icon
  static const IconData adobephotoshop =
      IconData(0xe980, fontFamily: _fontFamily, fontPackage: "mbi");

  /// adobepremiere icon
  static const IconData adobepremiere =
      IconData(0xe981, fontFamily: _fontFamily, fontPackage: "mbi");

  /// adobetypekit icon
  static const IconData adobetypekit =
      IconData(0xe982, fontFamily: _fontFamily, fontPackage: "mbi");

  /// adobexd icon
  static const IconData adobexd =
      IconData(0xe983, fontFamily: _fontFamily, fontPackage: "mbi");

  /// airbnb icon
  static const IconData airbnb =
      IconData(0xe984, fontFamily: _fontFamily, fontPackage: "mbi");

  /// airplayaudio icon
  static const IconData airplayaudio =
      IconData(0xe985, fontFamily: _fontFamily, fontPackage: "mbi");

  /// airplayvideo icon
  static const IconData airplayvideo =
      IconData(0xe986, fontFamily: _fontFamily, fontPackage: "mbi");

  /// algolia icon
  static const IconData algolia =
      IconData(0xe987, fontFamily: _fontFamily, fontPackage: "mbi");

  /// alliedmodders icon
  static const IconData alliedmodders =
      IconData(0xe988, fontFamily: _fontFamily, fontPackage: "mbi");

  /// amazon1 icon
  static const IconData amazon1 =
      IconData(0xe989, fontFamily: _fontFamily, fontPackage: "mbi");

  /// amazonalexa icon
  static const IconData amazonalexa =
      IconData(0xe98a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// amazonaws icon
  static const IconData amazonaws =
      IconData(0xe98b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// amd icon
  static const IconData amd =
      IconData(0xe98c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// americanexpress icon
  static const IconData americanexpress =
      IconData(0xe98d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// anaconda icon
  static const IconData anaconda =
      IconData(0xe98e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// analogue icon
  static const IconData analogue =
      IconData(0xe98f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// anchor icon
  static const IconData anchor =
      IconData(0xe990, fontFamily: _fontFamily, fontPackage: "mbi");

  /// android1 icon
  static const IconData android1 =
      IconData(0xe991, fontFamily: _fontFamily, fontPackage: "mbi");

  /// angellist icon
  static const IconData angellist =
      IconData(0xe992, fontFamily: _fontFamily, fontPackage: "mbi");

  /// angular icon
  static const IconData angular =
      IconData(0xe993, fontFamily: _fontFamily, fontPackage: "mbi");

  /// angularuniversal icon
  static const IconData angularuniversal =
      IconData(0xe994, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ansible icon
  static const IconData ansible =
      IconData(0xe995, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apache icon
  static const IconData apache =
      IconData(0xe996, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apacheairflow icon
  static const IconData apacheairflow =
      IconData(0xe997, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apachecordova icon
  static const IconData apachecordova =
      IconData(0xe998, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apacheflink icon
  static const IconData apacheflink =
      IconData(0xe999, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apachekafka icon
  static const IconData apachekafka =
      IconData(0xe99a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apachenetbeanside icon
  static const IconData apachenetbeanside =
      IconData(0xe99b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apacheopenoffice icon
  static const IconData apacheopenoffice =
      IconData(0xe99c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apacherocketmq icon
  static const IconData apacherocketmq =
      IconData(0xe99d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apachespark icon
  static const IconData apachespark =
      IconData(0xe99e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// apple icon
  static const IconData apple =
      IconData(0xe99f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// applemusic icon
  static const IconData applemusic =
      IconData(0xe9a0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// applepay icon
  static const IconData applepay =
      IconData(0xe9a1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// applepodcasts icon
  static const IconData applepodcasts =
      IconData(0xe9a2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// appveyor icon
  static const IconData appveyor =
      IconData(0xe9a3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// aral icon
  static const IconData aral =
      IconData(0xe9a4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// archiveofourown icon
  static const IconData archiveofourown =
      IconData(0xe9a5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// archlinux icon
  static const IconData archlinux =
      IconData(0xe9a6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// arduino icon
  static const IconData arduino =
      IconData(0xe9a7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// artstation icon
  static const IconData artstation =
      IconData(0xe9a8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// arxiv icon
  static const IconData arxiv =
      IconData(0xe9a9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// asana icon
  static const IconData asana =
      IconData(0xe9aa, fontFamily: _fontFamily, fontPackage: "mbi");

  /// asciidoctor icon
  static const IconData asciidoctor =
      IconData(0xe9ab, fontFamily: _fontFamily, fontPackage: "mbi");

  /// at_and_t icon
  static const IconData at_and_t =
      IconData(0xe9ac, fontFamily: _fontFamily, fontPackage: "mbi");

  /// atlassian icon
  static const IconData atlassian =
      IconData(0xe9ad, fontFamily: _fontFamily, fontPackage: "mbi");

  /// atom icon
  static const IconData atom =
      IconData(0xe9ae, fontFamily: _fontFamily, fontPackage: "mbi");

  /// audi icon
  static const IconData audi =
      IconData(0xe9af, fontFamily: _fontFamily, fontPackage: "mbi");

  /// audible icon
  static const IconData audible =
      IconData(0xe9b0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// aurelia icon
  static const IconData aurelia =
      IconData(0xe9b1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// auth0 icon
  static const IconData auth0 =
      IconData(0xe9b2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// automatic icon
  static const IconData automatic =
      IconData(0xe9b3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// autotask icon
  static const IconData autotask =
      IconData(0xe9b4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// aventrix icon
  static const IconData aventrix =
      IconData(0xe9b5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// awesomewm icon
  static const IconData awesomewm =
      IconData(0xe9b6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// azureartifacts icon
  static const IconData azureartifacts =
      IconData(0xe9b7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// azuredevops icon
  static const IconData azuredevops =
      IconData(0xe9b8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// azurepipelines icon
  static const IconData azurepipelines =
      IconData(0xe9b9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// babel icon
  static const IconData babel =
      IconData(0xe9ba, fontFamily: _fontFamily, fontPackage: "mbi");

  /// baidu icon
  static const IconData baidu =
      IconData(0xe9bb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bamboo icon
  static const IconData bamboo =
      IconData(0xe9bc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bancontact icon
  static const IconData bancontact =
      IconData(0xe9bd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bandcamp icon
  static const IconData bandcamp =
      IconData(0xe9be, fontFamily: _fontFamily, fontPackage: "mbi");

  ///bandlab icon
  /// bandlab icon
  static const IconData bandlab =
      IconData(0xe9bf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// basecamp1 icon
  static const IconData basecamp1 =
      IconData(0xe9c0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bathasu icon
  static const IconData bathasu =
      IconData(0xe9c1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// beats icon
  static const IconData beats =
      IconData(0xe9c2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// beatsbydre icon
  static const IconData beatsbydre =
      IconData(0xe9c3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// behance1 icon
  static const IconData behance1 =
      IconData(0xe9c4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bigcartel icon
  static const IconData bigcartel =
      IconData(0xe9c5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bing icon
  static const IconData bing =
      IconData(0xe9c6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bit icon
  static const IconData bit =
      IconData(0xe9c7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bitbucket icon
  static const IconData bitbucket =
      IconData(0xe9c8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bitcoin icon
  static const IconData bitcoin =
      IconData(0xe9c9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bitdefender icon
  static const IconData bitdefender =
      IconData(0xe9ca, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bitly icon
  static const IconData bitly =
      IconData(0xe9cb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bitrise icon
  static const IconData bitrise =
      IconData(0xe9cc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// blackberry icon
  static const IconData blackberry =
      IconData(0xe9cd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// blender icon
  static const IconData blender =
      IconData(0xe9ce, fontFamily: _fontFamily, fontPackage: "mbi");

  /// blogger1 icon
  static const IconData blogger1 =
      IconData(0xe9cf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bmcsoftware icon
  static const IconData bmcsoftware =
      IconData(0xe9d0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// boeing icon
  static const IconData boeing =
      IconData(0xe9d1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// boost icon
  static const IconData boost =
      IconData(0xe9d2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bootstrap icon
  static const IconData bootstrap =
      IconData(0xe9d3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bower icon
  static const IconData bower =
      IconData(0xe9d4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// box1 icon
  static const IconData box1 =
      IconData(0xe9d5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// brand_dot_ai icon
  static const IconData brand_dot_ai =
      IconData(0xe9d6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// brandfolder icon
  static const IconData brandfolder =
      IconData(0xe9d7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// brave icon
  static const IconData brave =
      IconData(0xe9d8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// breaker icon
  static const IconData breaker =
      IconData(0xe9d9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// broadcom icon
  static const IconData broadcom =
      IconData(0xe9da, fontFamily: _fontFamily, fontPackage: "mbi");

  /// buddy icon
  static const IconData buddy =
      IconData(0xe9db, fontFamily: _fontFamily, fontPackage: "mbi");

  /// buffer icon
  static const IconData buffer =
      IconData(0xe9dc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// buymeacoffee icon
  static const IconData buymeacoffee =
      IconData(0xe9dd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// buzzfeed icon
  static const IconData buzzfeed =
      IconData(0xe9de, fontFamily: _fontFamily, fontPackage: "mbi");

  /// c icon
  static const IconData c =
      IconData(0xe9df, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cakephp icon
  static const IconData cakephp =
      IconData(0xe9e0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// campaignmonitor icon
  static const IconData campaignmonitor =
      IconData(0xe9e1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// canva icon
  static const IconData canva =
      IconData(0xe9e2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cashapp icon
  static const IconData cashapp =
      IconData(0xe9e3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cassandra icon
  static const IconData cassandra =
      IconData(0xe9e4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// castorama icon
  static const IconData castorama =
      IconData(0xe9e5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// castro icon
  static const IconData castro =
      IconData(0xe9e6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// centos icon
  static const IconData centos =
      IconData(0xe9e7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cevo icon
  static const IconData cevo =
      IconData(0xe9e8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// chase icon
  static const IconData chase =
      IconData(0xe9e9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// chef icon
  static const IconData chef =
      IconData(0xe9ea, fontFamily: _fontFamily, fontPackage: "mbi");

  /// circle icon
  static const IconData circle =
      IconData(0xe9eb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// circleci icon
  static const IconData circleci =
      IconData(0xe9ec, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cirrusci icon
  static const IconData cirrusci =
      IconData(0xe9ed, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cisco icon
  static const IconData cisco =
      IconData(0xe9ee, fontFamily: _fontFamily, fontPackage: "mbi");

  /// civicrm icon
  static const IconData civicrm =
      IconData(0xe9ef, fontFamily: _fontFamily, fontPackage: "mbi");

  /// clockify icon
  static const IconData clockify =
      IconData(0xe9f0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// clojure icon
  static const IconData clojure =
      IconData(0xe9f1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cloudbees icon
  static const IconData cloudbees =
      IconData(0xe9f2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cloudflare icon
  static const IconData cloudflare =
      IconData(0xe9f3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cmake icon
  static const IconData cmake =
      IconData(0xe9f4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// co_op icon
  static const IconData co_op =
      IconData(0xe9f5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codacy icon
  static const IconData codacy =
      IconData(0xe9f6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codecademy icon
  static const IconData codecademy =
      IconData(0xe9f7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codechef icon
  static const IconData codechef =
      IconData(0xe9f8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codeclimate icon
  static const IconData codeclimate =
      IconData(0xe9f9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codecov icon
  static const IconData codecov =
      IconData(0xe9fa, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codefactor icon
  static const IconData codefactor =
      IconData(0xe9fb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codeforces icon
  static const IconData codeforces =
      IconData(0xe9fc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codeigniter icon
  static const IconData codeigniter =
      IconData(0xe9fd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codepen1 icon
  static const IconData codepen1 =
      IconData(0xe9fe, fontFamily: _fontFamily, fontPackage: "mbi");

  /// coderwall icon
  static const IconData coderwall =
      IconData(0xe9ff, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codesandbox icon
  static const IconData codesandbox =
      IconData(0xea00, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codeship icon
  static const IconData codeship =
      IconData(0xea01, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codewars icon
  static const IconData codewars =
      IconData(0xea02, fontFamily: _fontFamily, fontPackage: "mbi");

  /// codio icon
  static const IconData codio =
      IconData(0xea03, fontFamily: _fontFamily, fontPackage: "mbi");

  /// coffeescript icon
  static const IconData coffeescript =
      IconData(0xea04, fontFamily: _fontFamily, fontPackage: "mbi");

  /// coinbase icon
  static const IconData coinbase =
      IconData(0xea05, fontFamily: _fontFamily, fontPackage: "mbi");

  /// commonworkflowlanguage icon
  static const IconData commonworkflowlanguage =
      IconData(0xea06, fontFamily: _fontFamily, fontPackage: "mbi");

  /// composer icon
  static const IconData composer =
      IconData(0xea07, fontFamily: _fontFamily, fontPackage: "mbi");

  /// compropago icon
  static const IconData compropago =
      IconData(0xea08, fontFamily: _fontFamily, fontPackage: "mbi");

  /// conda_forge icon
  static const IconData conda_forge =
      IconData(0xea09, fontFamily: _fontFamily, fontPackage: "mbi");

  /// conekta icon
  static const IconData conekta =
      IconData(0xea0a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// confluence icon
  static const IconData confluence =
      IconData(0xea0b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// coronarenderer icon
  static const IconData coronarenderer =
      IconData(0xea0c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// coursera icon
  static const IconData coursera =
      IconData(0xea0d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// coveralls icon
  static const IconData coveralls =
      IconData(0xea0e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cpanel icon
  static const IconData cpanel =
      IconData(0xea0f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cplusplus icon
  static const IconData cplusplus =
      IconData(0xea10, fontFamily: _fontFamily, fontPackage: "mbi");

  /// creativecommons icon
  static const IconData creativecommons =
      IconData(0xea11, fontFamily: _fontFamily, fontPackage: "mbi");

  /// crehana icon
  static const IconData crehana =
      IconData(0xea12, fontFamily: _fontFamily, fontPackage: "mbi");

  /// crunchbase icon
  static const IconData crunchbase =
      IconData(0xea13, fontFamily: _fontFamily, fontPackage: "mbi");

  /// crunchyroll icon
  static const IconData crunchyroll =
      IconData(0xea14, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cryengine icon
  static const IconData cryengine =
      IconData(0xea15, fontFamily: _fontFamily, fontPackage: "mbi");

  /// csharp icon
  static const IconData csharp =
      IconData(0xea16, fontFamily: _fontFamily, fontPackage: "mbi");

  /// css31 icon
  static const IconData css31 =
      IconData(0xea17, fontFamily: _fontFamily, fontPackage: "mbi");

  /// csswizardry icon
  static const IconData csswizardry =
      IconData(0xea18, fontFamily: _fontFamily, fontPackage: "mbi");

  /// curl icon
  static const IconData curl =
      IconData(0xea19, fontFamily: _fontFamily, fontPackage: "mbi");

  /// d3_dot_js icon
  static const IconData d3_dot_js =
      IconData(0xea1a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dailymotion icon
  static const IconData dailymotion =
      IconData(0xea1b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dart icon
  static const IconData dart =
      IconData(0xea1c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dashlane icon
  static const IconData dashlane =
      IconData(0xea1d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// datacamp icon
  static const IconData datacamp =
      IconData(0xea1e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dazn icon
  static const IconData dazn =
      IconData(0xea1f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dblp icon
  static const IconData dblp =
      IconData(0xea20, fontFamily: _fontFamily, fontPackage: "mbi");

  /// debian icon
  static const IconData debian =
      IconData(0xea21, fontFamily: _fontFamily, fontPackage: "mbi");

  /// deepin icon
  static const IconData deepin =
      IconData(0xea22, fontFamily: _fontFamily, fontPackage: "mbi");

  /// deezer icon
  static const IconData deezer =
      IconData(0xea23, fontFamily: _fontFamily, fontPackage: "mbi");

  /// delicious1 icon
  static const IconData delicious1 =
      IconData(0xea24, fontFamily: _fontFamily, fontPackage: "mbi");

  /// deliveroo icon
  static const IconData deliveroo =
      IconData(0xea25, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dell icon
  static const IconData dell =
      IconData(0xea26, fontFamily: _fontFamily, fontPackage: "mbi");

  /// deno icon
  static const IconData deno =
      IconData(0xea27, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dependabot icon
  static const IconData dependabot =
      IconData(0xea28, fontFamily: _fontFamily, fontPackage: "mbi");

  /// designernews icon
  static const IconData designernews =
      IconData(0xea29, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dev_dot_to icon
  static const IconData dev_dot_to =
      IconData(0xea2a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// deviantart1 icon
  static const IconData deviantart1 =
      IconData(0xea2b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// devrant icon
  static const IconData devrant =
      IconData(0xea2c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// diaspora icon
  static const IconData diaspora =
      IconData(0xea2d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// digg icon
  static const IconData digg =
      IconData(0xea2e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// digitalocean icon
  static const IconData digitalocean =
      IconData(0xea2f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// directus icon
  static const IconData directus =
      IconData(0xea30, fontFamily: _fontFamily, fontPackage: "mbi");

  /// discord icon
  static const IconData discord =
      IconData(0xea31, fontFamily: _fontFamily, fontPackage: "mbi");

  /// discourse icon
  static const IconData discourse =
      IconData(0xea32, fontFamily: _fontFamily, fontPackage: "mbi");

  /// discover icon
  static const IconData discover =
      IconData(0xea33, fontFamily: _fontFamily, fontPackage: "mbi");

  /// disqus icon
  static const IconData disqus =
      IconData(0xea34, fontFamily: _fontFamily, fontPackage: "mbi");

  /// disroot icon
  static const IconData disroot =
      IconData(0xea35, fontFamily: _fontFamily, fontPackage: "mbi");

  /// django icon
  static const IconData django =
      IconData(0xea36, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dlna icon
  static const IconData dlna =
      IconData(0xea37, fontFamily: _fontFamily, fontPackage: "mbi");

  /// docker icon
  static const IconData docker =
      IconData(0xea38, fontFamily: _fontFamily, fontPackage: "mbi");

  /// docusign icon
  static const IconData docusign =
      IconData(0xea39, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dolby icon
  static const IconData dolby =
      IconData(0xea3a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dot_net icon
  static const IconData dot_net =
      IconData(0xea3b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// draugiem_dot_lv icon
  static const IconData draugiem_dot_lv =
      IconData(0xea3c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dribbble1 icon
  static const IconData dribbble1 =
      IconData(0xea3d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// drone icon
  static const IconData drone =
      IconData(0xea3e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dropbox1 icon
  static const IconData dropbox1 =
      IconData(0xea3f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// drupal icon
  static const IconData drupal =
      IconData(0xea40, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dtube icon
  static const IconData dtube =
      IconData(0xea41, fontFamily: _fontFamily, fontPackage: "mbi");

  /// duckduckgo icon
  static const IconData duckduckgo =
      IconData(0xea42, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dunked icon
  static const IconData dunked =
      IconData(0xea43, fontFamily: _fontFamily, fontPackage: "mbi");

  /// duolingo icon
  static const IconData duolingo =
      IconData(0xea44, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dynatrace icon
  static const IconData dynatrace =
      IconData(0xea45, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ebay icon
  static const IconData ebay =
      IconData(0xea46, fontFamily: _fontFamily, fontPackage: "mbi");

  /// eclipseide icon
  static const IconData eclipseide =
      IconData(0xea47, fontFamily: _fontFamily, fontPackage: "mbi");

  /// elastic icon
  static const IconData elastic =
      IconData(0xea48, fontFamily: _fontFamily, fontPackage: "mbi");

  /// elasticcloud icon
  static const IconData elasticcloud =
      IconData(0xea49, fontFamily: _fontFamily, fontPackage: "mbi");

  /// elasticsearch icon
  static const IconData elasticsearch =
      IconData(0xea4a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// elasticstack icon
  static const IconData elasticstack =
      IconData(0xea4b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// electron icon
  static const IconData electron =
      IconData(0xea4c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// elementary icon
  static const IconData elementary =
      IconData(0xea4d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// eleventy icon
  static const IconData eleventy =
      IconData(0xea4e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ello1 icon
  static const IconData ello1 =
      IconData(0xea4f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// elsevier icon
  static const IconData elsevier =
      IconData(0xea50, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ember_dot_js icon
  static const IconData ember_dot_js =
      IconData(0xea51, fontFamily: _fontFamily, fontPackage: "mbi");

  /// emlakjet icon
  static const IconData emlakjet =
      IconData(0xea52, fontFamily: _fontFamily, fontPackage: "mbi");

  /// empirekred icon
  static const IconData empirekred =
      IconData(0xea53, fontFamily: _fontFamily, fontPackage: "mbi");

  /// envato icon
  static const IconData envato =
      IconData(0xea54, fontFamily: _fontFamily, fontPackage: "mbi");

  /// epel icon
  static const IconData epel =
      IconData(0xea55, fontFamily: _fontFamily, fontPackage: "mbi");

  /// epicgames icon
  static const IconData epicgames =
      IconData(0xea56, fontFamily: _fontFamily, fontPackage: "mbi");

  /// epson icon
  static const IconData epson =
      IconData(0xea57, fontFamily: _fontFamily, fontPackage: "mbi");

  /// esea icon
  static const IconData esea =
      IconData(0xea58, fontFamily: _fontFamily, fontPackage: "mbi");

  /// eslint icon
  static const IconData eslint =
      IconData(0xea59, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ethereum icon
  static const IconData ethereum =
      IconData(0xea5a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// etsy icon
  static const IconData etsy =
      IconData(0xea5b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// eventbrite icon
  static const IconData eventbrite =
      IconData(0xea5c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// eventstore icon
  static const IconData eventstore =
      IconData(0xea5d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// evernote icon
  static const IconData evernote =
      IconData(0xea5e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// everplaces icon
  static const IconData everplaces =
      IconData(0xea5f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// evry icon
  static const IconData evry =
      IconData(0xea60, fontFamily: _fontFamily, fontPackage: "mbi");

  /// exercism icon
  static const IconData exercism =
      IconData(0xea61, fontFamily: _fontFamily, fontPackage: "mbi");

  /// expertsexchange icon
  static const IconData expertsexchange =
      IconData(0xea62, fontFamily: _fontFamily, fontPackage: "mbi");

  /// expo icon
  static const IconData expo =
      IconData(0xea63, fontFamily: _fontFamily, fontPackage: "mbi");

  /// eyeem icon
  static const IconData eyeem =
      IconData(0xea64, fontFamily: _fontFamily, fontPackage: "mbi");

  /// f_droid icon
  static const IconData f_droid =
      IconData(0xea65, fontFamily: _fontFamily, fontPackage: "mbi");

  /// f_secure icon
  static const IconData f_secure =
      IconData(0xea66, fontFamily: _fontFamily, fontPackage: "mbi");

  /// facebook1 icon
  static const IconData facebook1 =
      IconData(0xea67, fontFamily: _fontFamily, fontPackage: "mbi");

  /// faceit icon
  static const IconData faceit =
      IconData(0xea68, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fandango icon
  static const IconData fandango =
      IconData(0xea69, fontFamily: _fontFamily, fontPackage: "mbi");

  /// favro icon
  static const IconData favro =
      IconData(0xea6a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// feathub icon
  static const IconData feathub =
      IconData(0xea6b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fedora icon
  static const IconData fedora =
      IconData(0xea6c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// feedly icon
  static const IconData feedly =
      IconData(0xea6d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fidoalliance icon
  static const IconData fidoalliance =
      IconData(0xea6e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// figma icon
  static const IconData figma =
      IconData(0xea6f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// filezilla icon
  static const IconData filezilla =
      IconData(0xea70, fontFamily: _fontFamily, fontPackage: "mbi");

  /// firebase icon
  static const IconData firebase =
      IconData(0xea71, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fitbit icon
  static const IconData fitbit =
      IconData(0xea72, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fiverr icon
  static const IconData fiverr =
      IconData(0xea73, fontFamily: _fontFamily, fontPackage: "mbi");

  /// flask icon
  static const IconData flask =
      IconData(0xea74, fontFamily: _fontFamily, fontPackage: "mbi");

  /// flattr1 icon
  static const IconData flattr1 =
      IconData(0xea75, fontFamily: _fontFamily, fontPackage: "mbi");

  /// flickr1 icon
  static const IconData flickr1 =
      IconData(0xea76, fontFamily: _fontFamily, fontPackage: "mbi");

  /// flipboard icon
  static const IconData flipboard =
      IconData(0xea77, fontFamily: _fontFamily, fontPackage: "mbi");

  /// floatplane icon
  static const IconData floatplane =
      IconData(0xea78, fontFamily: _fontFamily, fontPackage: "mbi");

  /// flutter icon
  static const IconData flutter =
      IconData(0xea79, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fnac icon
  static const IconData fnac =
      IconData(0xea7a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// formstack icon
  static const IconData formstack =
      IconData(0xea7b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fossa icon
  static const IconData fossa =
      IconData(0xea7c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fossilscm icon
  static const IconData fossilscm =
      IconData(0xea7d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// foursquare1 icon
  static const IconData foursquare1 =
      IconData(0xea7e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// framer icon
  static const IconData framer =
      IconData(0xea7f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// freebsd icon
  static const IconData freebsd =
      IconData(0xea80, fontFamily: _fontFamily, fontPackage: "mbi");

  /// freecodecamp icon
  static const IconData freecodecamp =
      IconData(0xea81, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fujifilm icon
  static const IconData fujifilm =
      IconData(0xea82, fontFamily: _fontFamily, fontPackage: "mbi");

  /// fujitsu icon
  static const IconData fujitsu =
      IconData(0xea83, fontFamily: _fontFamily, fontPackage: "mbi");

  /// furaffinity icon
  static const IconData furaffinity =
      IconData(0xea84, fontFamily: _fontFamily, fontPackage: "mbi");

  /// furrynetwork icon
  static const IconData furrynetwork =
      IconData(0xea85, fontFamily: _fontFamily, fontPackage: "mbi");

  /// garmin icon
  static const IconData garmin =
      IconData(0xea86, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gatsby icon
  static const IconData gatsby =
      IconData(0xea87, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gauges icon
  static const IconData gauges =
      IconData(0xea88, fontFamily: _fontFamily, fontPackage: "mbi");

  /// genius icon
  static const IconData genius =
      IconData(0xea89, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gentoo icon
  static const IconData gentoo =
      IconData(0xea8a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// geocaching icon
  static const IconData geocaching =
      IconData(0xea8b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gerrit icon
  static const IconData gerrit =
      IconData(0xea8c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ghost icon
  static const IconData ghost =
      IconData(0xea8d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gimp icon
  static const IconData gimp =
      IconData(0xea8e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// git1 icon
  static const IconData git1 =
      IconData(0xea8f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gitea icon
  static const IconData gitea =
      IconData(0xea90, fontFamily: _fontFamily, fontPackage: "mbi");

  /// github1 icon
  static const IconData github1 =
      IconData(0xea91, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gitlab icon
  static const IconData gitlab =
      IconData(0xea92, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gitpod icon
  static const IconData gitpod =
      IconData(0xea93, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gitter icon
  static const IconData gitter =
      IconData(0xea94, fontFamily: _fontFamily, fontPackage: "mbi");

  /// glassdoor icon
  static const IconData glassdoor =
      IconData(0xea95, fontFamily: _fontFamily, fontPackage: "mbi");

  /// glitch icon
  static const IconData glitch =
      IconData(0xea96, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gmail icon
  static const IconData gmail =
      IconData(0xea97, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gnome icon
  static const IconData gnome =
      IconData(0xea98, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gnu icon
  static const IconData gnu =
      IconData(0xea99, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gnuicecat icon
  static const IconData gnuicecat =
      IconData(0xea9a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gnuprivacyguard icon
  static const IconData gnuprivacyguard =
      IconData(0xea9b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gnusocial icon
  static const IconData gnusocial =
      IconData(0xea9c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// go icon
  static const IconData go =
      IconData(0xea9d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// godotengine icon
  static const IconData godotengine =
      IconData(0xea9e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gog_dot_com icon
  static const IconData gog_dot_com =
      IconData(0xea9f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// goldenline icon
  static const IconData goldenline =
      IconData(0xeaa0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// goodreads icon
  static const IconData goodreads =
      IconData(0xeaa1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// google1 icon
  static const IconData google1 =
      IconData(0xeaa2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googleads icon
  static const IconData googleads =
      IconData(0xeaa3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googleanalytics icon
  static const IconData googleanalytics =
      IconData(0xeaa4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googlechrome icon
  static const IconData googlechrome =
      IconData(0xeaa5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googlecloud icon
  static const IconData googlecloud =
      IconData(0xeaa6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googledrive icon
  static const IconData googledrive =
      IconData(0xeaa7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googlehangouts icon
  static const IconData googlehangouts =
      IconData(0xeaa8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googlehangoutschat icon
  static const IconData googlehangoutschat =
      IconData(0xeaa9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googlekeep icon
  static const IconData googlekeep =
      IconData(0xeaaa, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googlepay icon
  static const IconData googlepay =
      IconData(0xeaab, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googleplay icon
  static const IconData googleplay =
      IconData(0xeaac, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googlepodcasts icon
  static const IconData googlepodcasts =
      IconData(0xeaad, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googlescholar icon
  static const IconData googlescholar =
      IconData(0xeaae, fontFamily: _fontFamily, fontPackage: "mbi");

  /// googlesearchconsole icon
  static const IconData googlesearchconsole =
      IconData(0xeaaf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gov_dot_uk icon
  static const IconData gov_dot_uk =
      IconData(0xeab0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gradle icon
  static const IconData gradle =
      IconData(0xeab1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// grafana icon
  static const IconData grafana =
      IconData(0xeab2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// graphcool icon
  static const IconData graphcool =
      IconData(0xeab3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// graphql icon
  static const IconData graphql =
      IconData(0xeab4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// grav icon
  static const IconData grav =
      IconData(0xeab5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gravatar icon
  static const IconData gravatar =
      IconData(0xeab6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// greenkeeper icon
  static const IconData greenkeeper =
      IconData(0xeab7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// greensock icon
  static const IconData greensock =
      IconData(0xeab8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// groovy icon
  static const IconData groovy =
      IconData(0xeab9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// groupon icon
  static const IconData groupon =
      IconData(0xeaba, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gulp icon
  static const IconData gulp =
      IconData(0xeabb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gumroad icon
  static const IconData gumroad =
      IconData(0xeabc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gumtree icon
  static const IconData gumtree =
      IconData(0xeabd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// gutenberg icon
  static const IconData gutenberg =
      IconData(0xeabe, fontFamily: _fontFamily, fontPackage: "mbi");

  /// habr icon
  static const IconData habr =
      IconData(0xeabf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hackaday icon
  static const IconData hackaday =
      IconData(0xeac0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hackerearth icon
  static const IconData hackerearth =
      IconData(0xeac1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hackerone icon
  static const IconData hackerone =
      IconData(0xeac2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hackerrank icon
  static const IconData hackerrank =
      IconData(0xeac3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hackhands icon
  static const IconData hackhands =
      IconData(0xeac4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hackster icon
  static const IconData hackster =
      IconData(0xeac5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// happycow icon
  static const IconData happycow =
      IconData(0xeac6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hashnode icon
  static const IconData hashnode =
      IconData(0xeac7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// haskell icon
  static const IconData haskell =
      IconData(0xeac8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hatenabookmark icon
  static const IconData hatenabookmark =
      IconData(0xeac9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// haxe icon
  static const IconData haxe =
      IconData(0xeaca, fontFamily: _fontFamily, fontPackage: "mbi");

  /// helm icon
  static const IconData helm =
      IconData(0xeacb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// here icon
  static const IconData here =
      IconData(0xeacc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// heroku icon
  static const IconData heroku =
      IconData(0xeacd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hexo icon
  static const IconData hexo =
      IconData(0xeace, fontFamily: _fontFamily, fontPackage: "mbi");

  /// highly icon
  static const IconData highly =
      IconData(0xeacf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hipchat icon
  static const IconData hipchat =
      IconData(0xead0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hitachi icon
  static const IconData hitachi =
      IconData(0xead1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hockeyapp icon
  static const IconData hockeyapp =
      IconData(0xead2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// homeassistant icon
  static const IconData homeassistant =
      IconData(0xead3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// homify icon
  static const IconData homify =
      IconData(0xead4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hootsuite icon
  static const IconData hootsuite =
      IconData(0xead5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// houzz icon
  static const IconData houzz =
      IconData(0xead6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hp icon
  static const IconData hp =
      IconData(0xead7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// html5 icon
  static const IconData html5 =
      IconData(0xead8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// htmlacademy icon
  static const IconData htmlacademy =
      IconData(0xead9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// huawei icon
  static const IconData huawei =
      IconData(0xeada, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hubspot icon
  static const IconData hubspot =
      IconData(0xeadb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hulu icon
  static const IconData hulu =
      IconData(0xeadc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// humblebundle icon
  static const IconData humblebundle =
      IconData(0xeadd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hurriyetemlak icon
  static const IconData hurriyetemlak =
      IconData(0xeade, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hypothesis icon
  static const IconData hypothesis =
      IconData(0xeadf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// iata icon
  static const IconData iata =
      IconData(0xeae0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ibm icon
  static const IconData ibm =
      IconData(0xeae1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// icloud icon
  static const IconData icloud =
      IconData(0xeae2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// icomoon icon
  static const IconData icomoon =
      IconData(0xeae3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// icon icon
  static const IconData icon =
      IconData(0xeae4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// iconjar icon
  static const IconData iconjar =
      IconData(0xeae5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// icq icon
  static const IconData icq =
      IconData(0xeae6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ideal icon
  static const IconData ideal =
      IconData(0xeae7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ifixit icon
  static const IconData ifixit =
      IconData(0xeae8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// imdb icon
  static const IconData imdb =
      IconData(0xeae9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// imgur icon
  static const IconData imgur =
      IconData(0xeaea, fontFamily: _fontFamily, fontPackage: "mbi");

  /// indeed icon
  static const IconData indeed =
      IconData(0xeaeb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// influxdb icon
  static const IconData influxdb =
      IconData(0xeaec, fontFamily: _fontFamily, fontPackage: "mbi");

  /// inkscape icon
  static const IconData inkscape =
      IconData(0xeaed, fontFamily: _fontFamily, fontPackage: "mbi");

  /// instacart icon
  static const IconData instacart =
      IconData(0xeaee, fontFamily: _fontFamily, fontPackage: "mbi");

  /// instagram1 icon
  static const IconData instagram1 =
      IconData(0xeaef, fontFamily: _fontFamily, fontPackage: "mbi");

  /// instapaper icon
  static const IconData instapaper =
      IconData(0xeaf0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// intel icon
  static const IconData intel =
      IconData(0xeaf1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// intellijidea icon
  static const IconData intellijidea =
      IconData(0xeaf2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// intercom icon
  static const IconData intercom =
      IconData(0xeaf3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// internetarchive icon
  static const IconData internetarchive =
      IconData(0xeaf4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// internetexplorer icon
  static const IconData internetexplorer =
      IconData(0xeaf5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// invision icon
  static const IconData invision =
      IconData(0xeaf6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// invoiceninja icon
  static const IconData invoiceninja =
      IconData(0xeaf7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ionic icon
  static const IconData ionic =
      IconData(0xeaf8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ios icon
  static const IconData ios =
      IconData(0xeaf9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ipfs icon
  static const IconData ipfs =
      IconData(0xeafa, fontFamily: _fontFamily, fontPackage: "mbi");

  /// issuu icon
  static const IconData issuu =
      IconData(0xeafb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// itch_dot_io icon
  static const IconData itch_dot_io =
      IconData(0xeafc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// itunes icon
  static const IconData itunes =
      IconData(0xeafd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jabber icon
  static const IconData jabber =
      IconData(0xeafe, fontFamily: _fontFamily, fontPackage: "mbi");

  /// java icon
  static const IconData java =
      IconData(0xeaff, fontFamily: _fontFamily, fontPackage: "mbi");

  /// javascript icon
  static const IconData javascript =
      IconData(0xeb00, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jekyll icon
  static const IconData jekyll =
      IconData(0xeb01, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jenkins icon
  static const IconData jenkins =
      IconData(0xeb02, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jest icon
  static const IconData jest =
      IconData(0xeb03, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jet icon
  static const IconData jet =
      IconData(0xeb04, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jetbrains icon
  static const IconData jetbrains =
      IconData(0xeb05, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jinja icon
  static const IconData jinja =
      IconData(0xeb06, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jira icon
  static const IconData jira =
      IconData(0xeb07, fontFamily: _fontFamily, fontPackage: "mbi");

  /// joomla1 icon
  static const IconData joomla1 =
      IconData(0xeb08, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jquery icon
  static const IconData jquery =
      IconData(0xeb09, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jsdelivr icon
  static const IconData jsdelivr =
      IconData(0xeb0a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jsfiddle icon
  static const IconData jsfiddle =
      IconData(0xeb0b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// json icon
  static const IconData json =
      IconData(0xeb0c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// jupyter icon
  static const IconData jupyter =
      IconData(0xeb0d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// justgiving icon
  static const IconData justgiving =
      IconData(0xeb0e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kaggle icon
  static const IconData kaggle =
      IconData(0xeb0f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kaios icon
  static const IconData kaios =
      IconData(0xeb10, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kaspersky icon
  static const IconData kaspersky =
      IconData(0xeb11, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kentico icon
  static const IconData kentico =
      IconData(0xeb12, fontFamily: _fontFamily, fontPackage: "mbi");

  /// keras icon
  static const IconData keras =
      IconData(0xeb13, fontFamily: _fontFamily, fontPackage: "mbi");

  /// keybase icon
  static const IconData keybase =
      IconData(0xeb14, fontFamily: _fontFamily, fontPackage: "mbi");

  /// keycdn icon
  static const IconData keycdn =
      IconData(0xeb15, fontFamily: _fontFamily, fontPackage: "mbi");

  /// khanacademy icon
  static const IconData khanacademy =
      IconData(0xeb16, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kibana icon
  static const IconData kibana =
      IconData(0xeb17, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kickstarter icon
  static const IconData kickstarter =
      IconData(0xeb18, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kik icon
  static const IconData kik =
      IconData(0xeb19, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kirby icon
  static const IconData kirby =
      IconData(0xeb1a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// klout icon
  static const IconData klout =
      IconData(0xeb1b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// known icon
  static const IconData known =
      IconData(0xeb1c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ko_fi icon
  static const IconData ko_fi =
      IconData(0xeb1d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kodi icon
  static const IconData kodi =
      IconData(0xeb1e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// koding icon
  static const IconData koding =
      IconData(0xeb1f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kotlin icon
  static const IconData kotlin =
      IconData(0xeb20, fontFamily: _fontFamily, fontPackage: "mbi");

  /// krita icon
  static const IconData krita =
      IconData(0xeb21, fontFamily: _fontFamily, fontPackage: "mbi");

  /// kubernetes icon
  static const IconData kubernetes =
      IconData(0xeb22, fontFamily: _fontFamily, fontPackage: "mbi");

  /// laravel icon
  static const IconData laravel =
      IconData(0xeb23, fontFamily: _fontFamily, fontPackage: "mbi");

  /// laravelhorizon icon
  static const IconData laravelhorizon =
      IconData(0xeb24, fontFamily: _fontFamily, fontPackage: "mbi");

  /// laravelnova icon
  static const IconData laravelnova =
      IconData(0xeb25, fontFamily: _fontFamily, fontPackage: "mbi");

  /// last_dot_fm icon
  static const IconData last_dot_fm =
      IconData(0xeb26, fontFamily: _fontFamily, fontPackage: "mbi");

  /// lastpass icon
  static const IconData lastpass =
      IconData(0xeb27, fontFamily: _fontFamily, fontPackage: "mbi");

  /// latex icon
  static const IconData latex =
      IconData(0xeb28, fontFamily: _fontFamily, fontPackage: "mbi");

  /// launchpad icon
  static const IconData launchpad =
      IconData(0xeb29, fontFamily: _fontFamily, fontPackage: "mbi");

  /// leetcode icon
  static const IconData leetcode =
      IconData(0xeb2a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// lenovo icon
  static const IconData lenovo =
      IconData(0xeb2b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// letsencrypt icon
  static const IconData letsencrypt =
      IconData(0xeb2c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// letterboxd icon
  static const IconData letterboxd =
      IconData(0xeb2d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// lgtm icon
  static const IconData lgtm =
      IconData(0xeb2e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// liberapay icon
  static const IconData liberapay =
      IconData(0xeb2f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// librarything icon
  static const IconData librarything =
      IconData(0xeb30, fontFamily: _fontFamily, fontPackage: "mbi");

  /// libreoffice1 icon
  static const IconData libreoffice1 =
      IconData(0xeb31, fontFamily: _fontFamily, fontPackage: "mbi");

  /// line icon
  static const IconData line =
      IconData(0xeb32, fontFamily: _fontFamily, fontPackage: "mbi");

  /// linewebtoon icon
  static const IconData linewebtoon =
      IconData(0xeb33, fontFamily: _fontFamily, fontPackage: "mbi");

  /// linkedin1 icon
  static const IconData linkedin1 =
      IconData(0xeb34, fontFamily: _fontFamily, fontPackage: "mbi");

  /// linode icon
  static const IconData linode =
      IconData(0xeb35, fontFamily: _fontFamily, fontPackage: "mbi");

  /// linux icon
  static const IconData linux =
      IconData(0xeb36, fontFamily: _fontFamily, fontPackage: "mbi");

  /// linuxfoundation icon
  static const IconData linuxfoundation =
      IconData(0xeb37, fontFamily: _fontFamily, fontPackage: "mbi");

  /// linuxmint icon
  static const IconData linuxmint =
      IconData(0xeb38, fontFamily: _fontFamily, fontPackage: "mbi");

  /// livejournal icon
  static const IconData livejournal =
      IconData(0xeb39, fontFamily: _fontFamily, fontPackage: "mbi");

  /// livestream icon
  static const IconData livestream =
      IconData(0xeb3a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// llvm icon
  static const IconData llvm =
      IconData(0xeb3b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// logstash icon
  static const IconData logstash =
      IconData(0xeb3c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// loop1 icon
  static const IconData loop1 =
      IconData(0xeb3d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// lua icon
  static const IconData lua =
      IconData(0xeb3e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// lufthansa icon
  static const IconData lufthansa =
      IconData(0xeb3f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// lumen icon
  static const IconData lumen =
      IconData(0xeb40, fontFamily: _fontFamily, fontPackage: "mbi");

  /// lyft icon
  static const IconData lyft =
      IconData(0xeb41, fontFamily: _fontFamily, fontPackage: "mbi");

  /// macys icon
  static const IconData macys =
      IconData(0xeb42, fontFamily: _fontFamily, fontPackage: "mbi");

  /// magento icon
  static const IconData magento =
      IconData(0xeb43, fontFamily: _fontFamily, fontPackage: "mbi");

  /// magisk icon
  static const IconData magisk =
      IconData(0xeb44, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mail_dot_ru icon
  static const IconData mail_dot_ru =
      IconData(0xeb45, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mailchimp icon
  static const IconData mailchimp =
      IconData(0xeb46, fontFamily: _fontFamily, fontPackage: "mbi");

  /// makerbot icon
  static const IconData makerbot =
      IconData(0xeb47, fontFamily: _fontFamily, fontPackage: "mbi");

  /// manageiq icon
  static const IconData manageiq =
      IconData(0xeb48, fontFamily: _fontFamily, fontPackage: "mbi");

  /// manjaro icon
  static const IconData manjaro =
      IconData(0xeb49, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mapbox icon
  static const IconData mapbox =
      IconData(0xeb4a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// markdown icon
  static const IconData markdown =
      IconData(0xeb4b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// marketo icon
  static const IconData marketo =
      IconData(0xeb4c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mastercard icon
  static const IconData mastercard =
      IconData(0xeb4d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mastodon icon
  static const IconData mastodon =
      IconData(0xeb4e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// materialdesign icon
  static const IconData materialdesign =
      IconData(0xeb4f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mathworks icon
  static const IconData mathworks =
      IconData(0xeb50, fontFamily: _fontFamily, fontPackage: "mbi");

  /// matrix icon
  static const IconData matrix =
      IconData(0xeb51, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mattermost icon
  static const IconData mattermost =
      IconData(0xeb52, fontFamily: _fontFamily, fontPackage: "mbi");

  /// matternet icon
  static const IconData matternet =
      IconData(0xeb53, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mcafee icon
  static const IconData mcafee =
      IconData(0xeb54, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mdnwebdocs icon
  static const IconData mdnwebdocs =
      IconData(0xeb55, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mediafire icon
  static const IconData mediafire =
      IconData(0xeb56, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mediatemple icon
  static const IconData mediatemple =
      IconData(0xeb57, fontFamily: _fontFamily, fontPackage: "mbi");

  /// medium icon
  static const IconData medium =
      IconData(0xeb58, fontFamily: _fontFamily, fontPackage: "mbi");

  /// meetup icon
  static const IconData meetup =
      IconData(0xeb59, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mega icon
  static const IconData mega =
      IconData(0xeb5a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mendeley icon
  static const IconData mendeley =
      IconData(0xeb5b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mercedes icon
  static const IconData mercedes =
      IconData(0xeb5c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// messenger icon
  static const IconData messenger =
      IconData(0xeb5d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// meteor icon
  static const IconData meteor =
      IconData(0xeb5e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// micro_dot_blog icon
  static const IconData micro_dot_blog =
      IconData(0xeb5f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microgenetics icon
  static const IconData microgenetics =
      IconData(0xeb60, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoft icon
  static const IconData microsoft =
      IconData(0xeb61, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftaccess icon
  static const IconData microsoftaccess =
      IconData(0xeb62, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftazure icon
  static const IconData microsoftazure =
      IconData(0xeb63, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftedge icon
  static const IconData microsoftedge =
      IconData(0xeb64, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftexcel icon
  static const IconData microsoftexcel =
      IconData(0xeb65, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftoffice icon
  static const IconData microsoftoffice =
      IconData(0xeb66, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftonedrive icon
  static const IconData microsoftonedrive =
      IconData(0xeb67, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftonenote icon
  static const IconData microsoftonenote =
      IconData(0xeb68, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftoutlook icon
  static const IconData microsoftoutlook =
      IconData(0xeb69, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftpowerpoint icon
  static const IconData microsoftpowerpoint =
      IconData(0xeb6a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftteams icon
  static const IconData microsoftteams =
      IconData(0xeb6b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microsoftword icon
  static const IconData microsoftword =
      IconData(0xeb6c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microstrategy icon
  static const IconData microstrategy =
      IconData(0xeb6d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// minds icon
  static const IconData minds =
      IconData(0xeb6e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// minetest icon
  static const IconData minetest =
      IconData(0xeb6f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// minutemailer icon
  static const IconData minutemailer =
      IconData(0xeb70, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mix icon
  static const IconData mix =
      IconData(0xeb71, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mixcloud icon
  static const IconData mixcloud =
      IconData(0xeb72, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mixer icon
  static const IconData mixer =
      IconData(0xeb73, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mojang icon
  static const IconData mojang =
      IconData(0xeb74, fontFamily: _fontFamily, fontPackage: "mbi");

  /// monero icon
  static const IconData monero =
      IconData(0xeb75, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mongodb icon
  static const IconData mongodb =
      IconData(0xeb76, fontFamily: _fontFamily, fontPackage: "mbi");

  /// monkeytie icon
  static const IconData monkeytie =
      IconData(0xeb77, fontFamily: _fontFamily, fontPackage: "mbi");

  /// monogram icon
  static const IconData monogram =
      IconData(0xeb78, fontFamily: _fontFamily, fontPackage: "mbi");

  /// monster icon
  static const IconData monster =
      IconData(0xeb79, fontFamily: _fontFamily, fontPackage: "mbi");

  /// monzo icon
  static const IconData monzo =
      IconData(0xeb7a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// moo icon
  static const IconData moo =
      IconData(0xeb7b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mozilla icon
  static const IconData mozilla =
      IconData(0xeb7c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mozillafirefox icon
  static const IconData mozillafirefox =
      IconData(0xeb7d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// musescore icon
  static const IconData musescore =
      IconData(0xeb7e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mxlinux icon
  static const IconData mxlinux =
      IconData(0xeb7f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// myspace icon
  static const IconData myspace =
      IconData(0xeb80, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mysql icon
  static const IconData mysql =
      IconData(0xeb81, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nativescript icon
  static const IconData nativescript =
      IconData(0xeb82, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nec icon
  static const IconData nec =
      IconData(0xeb83, fontFamily: _fontFamily, fontPackage: "mbi");

  /// neo4j icon
  static const IconData neo4j =
      IconData(0xeb84, fontFamily: _fontFamily, fontPackage: "mbi");

  /// netflix icon
  static const IconData netflix =
      IconData(0xeb85, fontFamily: _fontFamily, fontPackage: "mbi");

  /// netlify icon
  static const IconData netlify =
      IconData(0xeb86, fontFamily: _fontFamily, fontPackage: "mbi");

  /// next_dot_js icon
  static const IconData next_dot_js =
      IconData(0xeb87, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nextcloud icon
  static const IconData nextcloud =
      IconData(0xeb88, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nextdoor icon
  static const IconData nextdoor =
      IconData(0xeb89, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nginx icon
  static const IconData nginx =
      IconData(0xeb8a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nim icon
  static const IconData nim =
      IconData(0xeb8b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nintendo icon
  static const IconData nintendo =
      IconData(0xeb8c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nintendo3ds icon
  static const IconData nintendo3ds =
      IconData(0xeb8d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nintendogamecube icon
  static const IconData nintendogamecube =
      IconData(0xeb8e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nintendoswitch icon
  static const IconData nintendoswitch =
      IconData(0xeb8f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// node_dot_js icon
  static const IconData node_dot_js =
      IconData(0xeb90, fontFamily: _fontFamily, fontPackage: "mbi");

  /// node_red icon
  static const IconData node_red =
      IconData(0xeb91, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nodemon icon
  static const IconData nodemon =
      IconData(0xeb92, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nokia icon
  static const IconData nokia =
      IconData(0xeb93, fontFamily: _fontFamily, fontPackage: "mbi");

  /// notion icon
  static const IconData notion =
      IconData(0xeb94, fontFamily: _fontFamily, fontPackage: "mbi");

  /// notist icon
  static const IconData notist =
      IconData(0xeb95, fontFamily: _fontFamily, fontPackage: "mbi");

  /// npm1 icon
  static const IconData npm1 =
      IconData(0xeb96, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nucleo icon
  static const IconData nucleo =
      IconData(0xeb97, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nuget icon
  static const IconData nuget =
      IconData(0xeb98, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nutanix icon
  static const IconData nutanix =
      IconData(0xeb99, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nuxt_dot_js icon
  static const IconData nuxt_dot_js =
      IconData(0xeb9a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// nvidia icon
  static const IconData nvidia =
      IconData(0xeb9b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// obsstudio icon
  static const IconData obsstudio =
      IconData(0xeb9c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ocaml icon
  static const IconData ocaml =
      IconData(0xeb9d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// octave icon
  static const IconData octave =
      IconData(0xeb9e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// octopusdeploy icon
  static const IconData octopusdeploy =
      IconData(0xeb9f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// oculus icon
  static const IconData oculus =
      IconData(0xeba0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// odnoklassniki icon
  static const IconData odnoklassniki =
      IconData(0xeba1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// openaccess icon
  static const IconData openaccess =
      IconData(0xeba2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// openapiinitiative icon
  static const IconData openapiinitiative =
      IconData(0xeba3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// opencollective icon
  static const IconData opencollective =
      IconData(0xeba4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// openid icon
  static const IconData openid =
      IconData(0xeba5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// opensourceinitiative icon
  static const IconData opensourceinitiative =
      IconData(0xeba6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// openssl icon
  static const IconData openssl =
      IconData(0xeba7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// openstreetmap icon
  static const IconData openstreetmap =
      IconData(0xeba8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// opensuse icon
  static const IconData opensuse =
      IconData(0xeba9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// openvpn icon
  static const IconData openvpn =
      IconData(0xebaa, fontFamily: _fontFamily, fontPackage: "mbi");

  /// opera1 icon
  static const IconData opera1 =
      IconData(0xebab, fontFamily: _fontFamily, fontPackage: "mbi");

  /// opsgenie icon
  static const IconData opsgenie =
      IconData(0xebac, fontFamily: _fontFamily, fontPackage: "mbi");

  /// oracle icon
  static const IconData oracle =
      IconData(0xebad, fontFamily: _fontFamily, fontPackage: "mbi");

  /// orcid icon
  static const IconData orcid =
      IconData(0xebae, fontFamily: _fontFamily, fontPackage: "mbi");

  /// origin icon
  static const IconData origin =
      IconData(0xebaf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// osmc icon
  static const IconData osmc =
      IconData(0xebb0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// overcast icon
  static const IconData overcast =
      IconData(0xebb1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// overleaf icon
  static const IconData overleaf =
      IconData(0xebb2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ovh icon
  static const IconData ovh =
      IconData(0xebb3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pagekit icon
  static const IconData pagekit =
      IconData(0xebb4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// palantir icon
  static const IconData palantir =
      IconData(0xebb5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// paloaltosoftware icon
  static const IconData paloaltosoftware =
      IconData(0xebb6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pandora icon
  static const IconData pandora =
      IconData(0xebb7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pantheon icon
  static const IconData pantheon =
      IconData(0xebb8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// parse_dot_ly icon
  static const IconData parse_dot_ly =
      IconData(0xebb9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pastebin icon
  static const IconData pastebin =
      IconData(0xebba, fontFamily: _fontFamily, fontPackage: "mbi");

  /// patreon icon
  static const IconData patreon =
      IconData(0xebbb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// paypal1 icon
  static const IconData paypal1 =
      IconData(0xebbc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// peertube icon
  static const IconData peertube =
      IconData(0xebbd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// periscope icon
  static const IconData periscope =
      IconData(0xebbe, fontFamily: _fontFamily, fontPackage: "mbi");

  /// php icon
  static const IconData php =
      IconData(0xebbf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pi_hole icon
  static const IconData pi_hole =
      IconData(0xebc0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// picarto_dot_tv icon
  static const IconData picarto_dot_tv =
      IconData(0xebc1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pinboard icon
  static const IconData pinboard =
      IconData(0xebc2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pingdom icon
  static const IconData pingdom =
      IconData(0xebc3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pingup icon
  static const IconData pingup =
      IconData(0xebc4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pinterest1 icon
  static const IconData pinterest1 =
      IconData(0xebc5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pivotaltracker icon
  static const IconData pivotaltracker =
      IconData(0xebc6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pjsip icon
  static const IconData pjsip =
      IconData(0xebc7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// plangrid icon
  static const IconData plangrid =
      IconData(0xebc8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// player_dot_me icon
  static const IconData player_dot_me =
      IconData(0xebc9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// playerfm icon
  static const IconData playerfm =
      IconData(0xebca, fontFamily: _fontFamily, fontPackage: "mbi");

  /// playstation icon
  static const IconData playstation =
      IconData(0xebcb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// playstation3 icon
  static const IconData playstation3 =
      IconData(0xebcc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// playstation4 icon
  static const IconData playstation4 =
      IconData(0xebcd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// plesk icon
  static const IconData plesk =
      IconData(0xebce, fontFamily: _fontFamily, fontPackage: "mbi");

  /// plex icon
  static const IconData plex =
      IconData(0xebcf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pluralsight icon
  static const IconData pluralsight =
      IconData(0xebd0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// plurk icon
  static const IconData plurk =
      IconData(0xebd1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pocket icon
  static const IconData pocket =
      IconData(0xebd2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pocketcasts icon
  static const IconData pocketcasts =
      IconData(0xebd3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// postgresql icon
  static const IconData postgresql =
      IconData(0xebd4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// postman icon
  static const IconData postman =
      IconData(0xebd5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// postwoman icon
  static const IconData postwoman =
      IconData(0xebd6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// powershell icon
  static const IconData powershell =
      IconData(0xebd7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// prestashop icon
  static const IconData prestashop =
      IconData(0xebd8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// prettier icon
  static const IconData prettier =
      IconData(0xebd9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// prismic icon
  static const IconData prismic =
      IconData(0xebda, fontFamily: _fontFamily, fontPackage: "mbi");

  /// probot icon
  static const IconData probot =
      IconData(0xebdb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// processwire icon
  static const IconData processwire =
      IconData(0xebdc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// producthunt icon
  static const IconData producthunt =
      IconData(0xebdd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// prometheus icon
  static const IconData prometheus =
      IconData(0xebde, fontFamily: _fontFamily, fontPackage: "mbi");

  /// proto_dot_io icon
  static const IconData proto_dot_io =
      IconData(0xebdf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// protonmail icon
  static const IconData protonmail =
      IconData(0xebe0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// proxmox icon
  static const IconData proxmox =
      IconData(0xebe1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// publons icon
  static const IconData publons =
      IconData(0xebe2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// purescript icon
  static const IconData purescript =
      IconData(0xebe3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pypi icon
  static const IconData pypi =
      IconData(0xebe4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// python icon
  static const IconData python =
      IconData(0xebe5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pytorch icon
  static const IconData pytorch =
      IconData(0xebe6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pyup icon
  static const IconData pyup =
      IconData(0xebe7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// qemu icon
  static const IconData qemu =
      IconData(0xebe8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// qgis icon
  static const IconData qgis =
      IconData(0xebe9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// qiita icon
  static const IconData qiita =
      IconData(0xebea, fontFamily: _fontFamily, fontPackage: "mbi");

  /// qualcomm icon
  static const IconData qualcomm =
      IconData(0xebeb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// quantcast icon
  static const IconData quantcast =
      IconData(0xebec, fontFamily: _fontFamily, fontPackage: "mbi");

  /// quantopian icon
  static const IconData quantopian =
      IconData(0xebed, fontFamily: _fontFamily, fontPackage: "mbi");

  /// quarkus icon
  static const IconData quarkus =
      IconData(0xebee, fontFamily: _fontFamily, fontPackage: "mbi");

  /// quicktime icon
  static const IconData quicktime =
      IconData(0xebef, fontFamily: _fontFamily, fontPackage: "mbi");

  /// quip icon
  static const IconData quip =
      IconData(0xebf0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// quora icon
  static const IconData quora =
      IconData(0xebf1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// qwiklabs icon
  static const IconData qwiklabs =
      IconData(0xebf2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// qzone icon
  static const IconData qzone =
      IconData(0xebf3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// r icon
  static const IconData r =
      IconData(0xebf4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// rabbitmq icon
  static const IconData rabbitmq =
      IconData(0xebf5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// radiopublic icon
  static const IconData radiopublic =
      IconData(0xebf6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// rails icon
  static const IconData rails =
      IconData(0xebf7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// raspberrypi icon
  static const IconData raspberrypi =
      IconData(0xebf8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// react icon
  static const IconData react =
      IconData(0xebf9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// reactos icon
  static const IconData reactos =
      IconData(0xebfa, fontFamily: _fontFamily, fontPackage: "mbi");

  /// reactrouter icon
  static const IconData reactrouter =
      IconData(0xebfb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// readthedocs icon
  static const IconData readthedocs =
      IconData(0xebfc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// realm icon
  static const IconData realm =
      IconData(0xebfd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// reason icon
  static const IconData reason =
      IconData(0xebfe, fontFamily: _fontFamily, fontPackage: "mbi");

  /// reasonstudios icon
  static const IconData reasonstudios =
      IconData(0xebff, fontFamily: _fontFamily, fontPackage: "mbi");

  /// redbubble icon
  static const IconData redbubble =
      IconData(0xec00, fontFamily: _fontFamily, fontPackage: "mbi");

  /// reddit1 icon
  static const IconData reddit1 =
      IconData(0xec01, fontFamily: _fontFamily, fontPackage: "mbi");

  /// redhat icon
  static const IconData redhat =
      IconData(0xec02, fontFamily: _fontFamily, fontPackage: "mbi");

  /// redis icon
  static const IconData redis =
      IconData(0xec03, fontFamily: _fontFamily, fontPackage: "mbi");

  /// redux icon
  static const IconData redux =
      IconData(0xec04, fontFamily: _fontFamily, fontPackage: "mbi");

  /// renren1 icon
  static const IconData renren1 =
      IconData(0xec05, fontFamily: _fontFamily, fontPackage: "mbi");

  /// repl_dot_it icon
  static const IconData repl_dot_it =
      IconData(0xec06, fontFamily: _fontFamily, fontPackage: "mbi");

  /// researchgate icon
  static const IconData researchgate =
      IconData(0xec07, fontFamily: _fontFamily, fontPackage: "mbi");

  /// reverbnation icon
  static const IconData reverbnation =
      IconData(0xec08, fontFamily: _fontFamily, fontPackage: "mbi");

  /// riot icon
  static const IconData riot =
      IconData(0xec09, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ripple icon
  static const IconData ripple =
      IconData(0xec0a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// riseup icon
  static const IconData riseup =
      IconData(0xec0b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// rollup_dot_js icon
  static const IconData rollup_dot_js =
      IconData(0xec0c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// roots icon
  static const IconData roots =
      IconData(0xec0d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// roundcube icon
  static const IconData roundcube =
      IconData(0xec0e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// rss1 icon
  static const IconData rss1 =
      IconData(0xec0f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// rstudio icon
  static const IconData rstudio =
      IconData(0xec10, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ruby icon
  static const IconData ruby =
      IconData(0xec11, fontFamily: _fontFamily, fontPackage: "mbi");

  /// rubygems icon
  static const IconData rubygems =
      IconData(0xec12, fontFamily: _fontFamily, fontPackage: "mbi");

  /// runkeeper icon
  static const IconData runkeeper =
      IconData(0xec13, fontFamily: _fontFamily, fontPackage: "mbi");

  /// rust icon
  static const IconData rust =
      IconData(0xec14, fontFamily: _fontFamily, fontPackage: "mbi");

  /// safari1 icon
  static const IconData safari1 =
      IconData(0xec15, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sahibinden icon
  static const IconData sahibinden =
      IconData(0xec16, fontFamily: _fontFamily, fontPackage: "mbi");

  /// salesforce icon
  static const IconData salesforce =
      IconData(0xec17, fontFamily: _fontFamily, fontPackage: "mbi");

  /// saltstack icon
  static const IconData saltstack =
      IconData(0xec18, fontFamily: _fontFamily, fontPackage: "mbi");

  /// samsung icon
  static const IconData samsung =
      IconData(0xec19, fontFamily: _fontFamily, fontPackage: "mbi");

  /// samsungpay icon
  static const IconData samsungpay =
      IconData(0xec1a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sap icon
  static const IconData sap =
      IconData(0xec1b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sass icon
  static const IconData sass =
      IconData(0xec1c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// saucelabs icon
  static const IconData saucelabs =
      IconData(0xec1d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// scala icon
  static const IconData scala =
      IconData(0xec1e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// scaleway icon
  static const IconData scaleway =
      IconData(0xec1f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// scribd icon
  static const IconData scribd =
      IconData(0xec20, fontFamily: _fontFamily, fontPackage: "mbi");

  /// scrutinizerci icon
  static const IconData scrutinizerci =
      IconData(0xec21, fontFamily: _fontFamily, fontPackage: "mbi");

  /// seagate icon
  static const IconData seagate =
      IconData(0xec22, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sega icon
  static const IconData sega =
      IconData(0xec23, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sellfy icon
  static const IconData sellfy =
      IconData(0xec24, fontFamily: _fontFamily, fontPackage: "mbi");

  /// semaphoreci icon
  static const IconData semaphoreci =
      IconData(0xec25, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sensu icon
  static const IconData sensu =
      IconData(0xec26, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sentry icon
  static const IconData sentry =
      IconData(0xec27, fontFamily: _fontFamily, fontPackage: "mbi");

  /// serverfault icon
  static const IconData serverfault =
      IconData(0xec28, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shazam icon
  static const IconData shazam =
      IconData(0xec29, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shell icon
  static const IconData shell =
      IconData(0xec2a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shopify icon
  static const IconData shopify =
      IconData(0xec2b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// showpad icon
  static const IconData showpad =
      IconData(0xec2c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// siemens icon
  static const IconData siemens =
      IconData(0xec2d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// signal icon
  static const IconData signal =
      IconData(0xec2e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// simpleicons icon
  static const IconData simpleicons =
      IconData(0xec2f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sinaweibo icon
  static const IconData sinaweibo =
      IconData(0xec30, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sitepoint icon
  static const IconData sitepoint =
      IconData(0xec31, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sketch icon
  static const IconData sketch =
      IconData(0xec32, fontFamily: _fontFamily, fontPackage: "mbi");

  /// skillshare icon
  static const IconData skillshare =
      IconData(0xec33, fontFamily: _fontFamily, fontPackage: "mbi");

  /// skyliner icon
  static const IconData skyliner =
      IconData(0xec34, fontFamily: _fontFamily, fontPackage: "mbi");

  /// skype1 icon
  static const IconData skype1 =
      IconData(0xec35, fontFamily: _fontFamily, fontPackage: "mbi");

  /// slack icon
  static const IconData slack =
      IconData(0xec36, fontFamily: _fontFamily, fontPackage: "mbi");

  /// slashdot icon
  static const IconData slashdot =
      IconData(0xec37, fontFamily: _fontFamily, fontPackage: "mbi");

  /// slickpic icon
  static const IconData slickpic =
      IconData(0xec38, fontFamily: _fontFamily, fontPackage: "mbi");

  /// slides icon
  static const IconData slides =
      IconData(0xec39, fontFamily: _fontFamily, fontPackage: "mbi");

  /// smashingmagazine icon
  static const IconData smashingmagazine =
      IconData(0xec3a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// smugmug icon
  static const IconData smugmug =
      IconData(0xec3b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// snapchat icon
  static const IconData snapchat =
      IconData(0xec3c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// snapcraft icon
  static const IconData snapcraft =
      IconData(0xec3d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// snyk icon
  static const IconData snyk =
      IconData(0xec3e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// society6 icon
  static const IconData society6 =
      IconData(0xec3f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// socket_dot_io icon
  static const IconData socket_dot_io =
      IconData(0xec40, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sogou icon
  static const IconData sogou =
      IconData(0xec41, fontFamily: _fontFamily, fontPackage: "mbi");

  /// solus icon
  static const IconData solus =
      IconData(0xec42, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sonarcloud icon
  static const IconData sonarcloud =
      IconData(0xec43, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sonarlint icon
  static const IconData sonarlint =
      IconData(0xec44, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sonarqube icon
  static const IconData sonarqube =
      IconData(0xec45, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sonarsource icon
  static const IconData sonarsource =
      IconData(0xec46, fontFamily: _fontFamily, fontPackage: "mbi");

  /// songkick icon
  static const IconData songkick =
      IconData(0xec47, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sonicwall icon
  static const IconData sonicwall =
      IconData(0xec48, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sonos icon
  static const IconData sonos =
      IconData(0xec49, fontFamily: _fontFamily, fontPackage: "mbi");

  /// soundcloud1 icon
  static const IconData soundcloud1 =
      IconData(0xec4a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sourceengine icon
  static const IconData sourceengine =
      IconData(0xec4b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sourceforge icon
  static const IconData sourceforge =
      IconData(0xec4c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sourcegraph icon
  static const IconData sourcegraph =
      IconData(0xec4d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// spacemacs icon
  static const IconData spacemacs =
      IconData(0xec4e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// spacex icon
  static const IconData spacex =
      IconData(0xec4f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sparkfun icon
  static const IconData sparkfun =
      IconData(0xec50, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sparkpost icon
  static const IconData sparkpost =
      IconData(0xec51, fontFamily: _fontFamily, fontPackage: "mbi");

  /// spdx icon
  static const IconData spdx =
      IconData(0xec52, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speakerdeck icon
  static const IconData speakerdeck =
      IconData(0xec53, fontFamily: _fontFamily, fontPackage: "mbi");

  /// spectrum icon
  static const IconData spectrum =
      IconData(0xec54, fontFamily: _fontFamily, fontPackage: "mbi");

  /// spotify1 icon
  static const IconData spotify1 =
      IconData(0xec55, fontFamily: _fontFamily, fontPackage: "mbi");

  /// spotlight icon
  static const IconData spotlight =
      IconData(0xec56, fontFamily: _fontFamily, fontPackage: "mbi");

  /// spreaker icon
  static const IconData spreaker =
      IconData(0xec57, fontFamily: _fontFamily, fontPackage: "mbi");

  /// spring icon
  static const IconData spring =
      IconData(0xec58, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sprint icon
  static const IconData sprint =
      IconData(0xec59, fontFamily: _fontFamily, fontPackage: "mbi");

  /// square icon
  static const IconData square =
      IconData(0xec5a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// squarespace icon
  static const IconData squarespace =
      IconData(0xec5b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stackbit icon
  static const IconData stackbit =
      IconData(0xec5c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stackexchange icon
  static const IconData stackexchange =
      IconData(0xec5d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stackoverflow1 icon
  static const IconData stackoverflow1 =
      IconData(0xec5e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stackpath icon
  static const IconData stackpath =
      IconData(0xec5f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stackshare icon
  static const IconData stackshare =
      IconData(0xec60, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stadia icon
  static const IconData stadia =
      IconData(0xec61, fontFamily: _fontFamily, fontPackage: "mbi");

  /// statamic icon
  static const IconData statamic =
      IconData(0xec62, fontFamily: _fontFamily, fontPackage: "mbi");

  /// staticman icon
  static const IconData staticman =
      IconData(0xec63, fontFamily: _fontFamily, fontPackage: "mbi");

  /// statuspage icon
  static const IconData statuspage =
      IconData(0xec64, fontFamily: _fontFamily, fontPackage: "mbi");

  /// steam1 icon
  static const IconData steam1 =
      IconData(0xec65, fontFamily: _fontFamily, fontPackage: "mbi");

  /// steamworks icon
  static const IconData steamworks =
      IconData(0xec66, fontFamily: _fontFamily, fontPackage: "mbi");

  /// steem icon
  static const IconData steem =
      IconData(0xec67, fontFamily: _fontFamily, fontPackage: "mbi");

  /// steemit icon
  static const IconData steemit =
      IconData(0xec68, fontFamily: _fontFamily, fontPackage: "mbi");

  /// steinberg icon
  static const IconData steinberg =
      IconData(0xec69, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stencyl icon
  static const IconData stencyl =
      IconData(0xec6a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stitcher icon
  static const IconData stitcher =
      IconData(0xec6b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// storify icon
  static const IconData storify =
      IconData(0xec6c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// storybook icon
  static const IconData storybook =
      IconData(0xec6d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// strapi icon
  static const IconData strapi =
      IconData(0xec6e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// strava icon
  static const IconData strava =
      IconData(0xec6f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stripe icon
  static const IconData stripe =
      IconData(0xec70, fontFamily: _fontFamily, fontPackage: "mbi");

  /// strongswan icon
  static const IconData strongswan =
      IconData(0xec71, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stubhub icon
  static const IconData stubhub =
      IconData(0xec72, fontFamily: _fontFamily, fontPackage: "mbi");

  /// styled_components icon
  static const IconData styled_components =
      IconData(0xec73, fontFamily: _fontFamily, fontPackage: "mbi");

  /// styleshare icon
  static const IconData styleshare =
      IconData(0xec74, fontFamily: _fontFamily, fontPackage: "mbi");

  /// stylus icon
  static const IconData stylus =
      IconData(0xec75, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sublimetext icon
  static const IconData sublimetext =
      IconData(0xec76, fontFamily: _fontFamily, fontPackage: "mbi");

  /// subversion icon
  static const IconData subversion =
      IconData(0xec77, fontFamily: _fontFamily, fontPackage: "mbi");

  /// superuser icon
  static const IconData superuser =
      IconData(0xec78, fontFamily: _fontFamily, fontPackage: "mbi");

  /// svelte icon
  static const IconData svelte =
      IconData(0xec79, fontFamily: _fontFamily, fontPackage: "mbi");

  /// svg1 icon
  static const IconData svg1 =
      IconData(0xec7a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// svgo icon
  static const IconData svgo =
      IconData(0xec7b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// swagger icon
  static const IconData swagger =
      IconData(0xec7c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// swarm icon
  static const IconData swarm =
      IconData(0xec7d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// swift icon
  static const IconData swift =
      IconData(0xec7e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// symantec icon
  static const IconData symantec =
      IconData(0xec7f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// symfony icon
  static const IconData symfony =
      IconData(0xec80, fontFamily: _fontFamily, fontPackage: "mbi");

  /// synology icon
  static const IconData synology =
      IconData(0xec81, fontFamily: _fontFamily, fontPackage: "mbi");

  /// t_mobile icon
  static const IconData t_mobile =
      IconData(0xec82, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tableau icon
  static const IconData tableau =
      IconData(0xec83, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tails icon
  static const IconData tails =
      IconData(0xec84, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tailwindcss icon
  static const IconData tailwindcss =
      IconData(0xec85, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tapas icon
  static const IconData tapas =
      IconData(0xec86, fontFamily: _fontFamily, fontPackage: "mbi");

  /// teamviewer icon
  static const IconData teamviewer =
      IconData(0xec87, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ted icon
  static const IconData ted =
      IconData(0xec88, fontFamily: _fontFamily, fontPackage: "mbi");

  /// teespring icon
  static const IconData teespring =
      IconData(0xec89, fontFamily: _fontFamily, fontPackage: "mbi");

  /// telegram1 icon
  static const IconData telegram1 =
      IconData(0xec8a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tencentqq icon
  static const IconData tencentqq =
      IconData(0xec8b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tencentweibo icon
  static const IconData tencentweibo =
      IconData(0xec8c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tensorflow icon
  static const IconData tensorflow =
      IconData(0xec8d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// teradata icon
  static const IconData teradata =
      IconData(0xec8e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// terraform icon
  static const IconData terraform =
      IconData(0xec8f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tesla icon
  static const IconData tesla =
      IconData(0xec90, fontFamily: _fontFamily, fontPackage: "mbi");

  /// themighty icon
  static const IconData themighty =
      IconData(0xec91, fontFamily: _fontFamily, fontPackage: "mbi");

  /// themoviedatabase icon
  static const IconData themoviedatabase =
      IconData(0xec92, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tidal icon
  static const IconData tidal =
      IconData(0xec93, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tiktok icon
  static const IconData tiktok =
      IconData(0xec94, fontFamily: _fontFamily, fontPackage: "mbi");

  /// timescale icon
  static const IconData timescale =
      IconData(0xec95, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tinder icon
  static const IconData tinder =
      IconData(0xec96, fontFamily: _fontFamily, fontPackage: "mbi");

  /// todoist icon
  static const IconData todoist =
      IconData(0xec97, fontFamily: _fontFamily, fontPackage: "mbi");

  /// toggl icon
  static const IconData toggl =
      IconData(0xec98, fontFamily: _fontFamily, fontPackage: "mbi");

  /// topcoder icon
  static const IconData topcoder =
      IconData(0xec99, fontFamily: _fontFamily, fontPackage: "mbi");

  /// toptal icon
  static const IconData toptal =
      IconData(0xec9a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tor icon
  static const IconData tor =
      IconData(0xec9b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// toshiba icon
  static const IconData toshiba =
      IconData(0xec9c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// trainerroad icon
  static const IconData trainerroad =
      IconData(0xec9d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// trakt icon
  static const IconData trakt =
      IconData(0xec9e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// travisci icon
  static const IconData travisci =
      IconData(0xec9f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// treehouse icon
  static const IconData treehouse =
      IconData(0xeca0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// trello1 icon
  static const IconData trello1 =
      IconData(0xeca1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// trendmicro icon
  static const IconData trendmicro =
      IconData(0xeca2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tripadvisor icon
  static const IconData tripadvisor =
      IconData(0xeca3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// trulia icon
  static const IconData trulia =
      IconData(0xeca4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// trustpilot icon
  static const IconData trustpilot =
      IconData(0xeca5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tumblr1 icon
  static const IconData tumblr1 =
      IconData(0xeca6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// turkishairlines icon
  static const IconData turkishairlines =
      IconData(0xeca7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// twilio icon
  static const IconData twilio =
      IconData(0xeca8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// twitch1 icon
  static const IconData twitch1 =
      IconData(0xeca9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// twitter1 icon
  static const IconData twitter1 =
      IconData(0xecaa, fontFamily: _fontFamily, fontPackage: "mbi");

  /// twoo icon
  static const IconData twoo =
      IconData(0xecab, fontFamily: _fontFamily, fontPackage: "mbi");

  /// typescript icon
  static const IconData typescript =
      IconData(0xecac, fontFamily: _fontFamily, fontPackage: "mbi");

  /// typo3 icon
  static const IconData typo3 =
      IconData(0xecad, fontFamily: _fontFamily, fontPackage: "mbi");

  /// uber icon
  static const IconData uber =
      IconData(0xecae, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ubereats icon
  static const IconData ubereats =
      IconData(0xecaf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ubisoft icon
  static const IconData ubisoft =
      IconData(0xecb0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ublockorigin icon
  static const IconData ublockorigin =
      IconData(0xecb1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ubuntu icon
  static const IconData ubuntu =
      IconData(0xecb2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// udacity icon
  static const IconData udacity =
      IconData(0xecb3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// udemy icon
  static const IconData udemy =
      IconData(0xecb4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// uikit icon
  static const IconData uikit =
      IconData(0xecb5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// umbraco icon
  static const IconData umbraco =
      IconData(0xecb6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// unity icon
  static const IconData unity =
      IconData(0xecb7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// unrealengine icon
  static const IconData unrealengine =
      IconData(0xecb8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// unsplash icon
  static const IconData unsplash =
      IconData(0xecb9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// untappd icon
  static const IconData untappd =
      IconData(0xecba, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upwork icon
  static const IconData upwork =
      IconData(0xecbb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// v icon
  static const IconData v =
      IconData(0xecbc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// v8 icon
  static const IconData v8 =
      IconData(0xecbd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vagrant icon
  static const IconData vagrant =
      IconData(0xecbe, fontFamily: _fontFamily, fontPackage: "mbi");

  /// valve icon
  static const IconData valve =
      IconData(0xecbf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// veeam icon
  static const IconData veeam =
      IconData(0xecc0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// venmo icon
  static const IconData venmo =
      IconData(0xecc1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// verizon icon
  static const IconData verizon =
      IconData(0xecc2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// viadeo icon
  static const IconData viadeo =
      IconData(0xecc3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// viber icon
  static const IconData viber =
      IconData(0xecc4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vim icon
  static const IconData vim =
      IconData(0xecc5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vimeo1 icon
  static const IconData vimeo1 =
      IconData(0xecc6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vine1 icon
  static const IconData vine1 =
      IconData(0xecc7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// virb icon
  static const IconData virb =
      IconData(0xecc8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// visa icon
  static const IconData visa =
      IconData(0xecc9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// visualstudio icon
  static const IconData visualstudio =
      IconData(0xecca, fontFamily: _fontFamily, fontPackage: "mbi");

  /// visualstudiocode icon
  static const IconData visualstudiocode =
      IconData(0xeccb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vk1 icon
  static const IconData vk1 =
      IconData(0xeccc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vlcmediaplayer icon
  static const IconData vlcmediaplayer =
      IconData(0xeccd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vodafone icon
  static const IconData vodafone =
      IconData(0xecce, fontFamily: _fontFamily, fontPackage: "mbi");

  /// volkswagen icon
  static const IconData volkswagen =
      IconData(0xeccf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vsco icon
  static const IconData vsco =
      IconData(0xecd0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// vue_dot_js icon
  static const IconData vue_dot_js =
      IconData(0xecd1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// w3c icon
  static const IconData w3c =
      IconData(0xecd2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wattpad icon
  static const IconData wattpad =
      IconData(0xecd3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// waze icon
  static const IconData waze =
      IconData(0xecd4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// weasyl icon
  static const IconData weasyl =
      IconData(0xecd5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// webauthn icon
  static const IconData webauthn =
      IconData(0xecd6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// webcomponents_dot_org icon
  static const IconData webcomponents_dot_org =
      IconData(0xecd7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// webmin icon
  static const IconData webmin =
      IconData(0xecd8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// webpack icon
  static const IconData webpack =
      IconData(0xecd9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// webstorm icon
  static const IconData webstorm =
      IconData(0xecda, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wechat icon
  static const IconData wechat =
      IconData(0xecdb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// whatsapp1 icon
  static const IconData whatsapp1 =
      IconData(0xecdc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wheniwork icon
  static const IconData wheniwork =
      IconData(0xecdd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// whitesource icon
  static const IconData whitesource =
      IconData(0xecde, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wii icon
  static const IconData wii =
      IconData(0xecdf, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wiiu icon
  static const IconData wiiu =
      IconData(0xece0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wikipedia1 icon
  static const IconData wikipedia1 =
      IconData(0xece1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// windows2 icon
  static const IconData windows2 =
      IconData(0xece2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wire icon
  static const IconData wire =
      IconData(0xece3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wireguard icon
  static const IconData wireguard =
      IconData(0xece4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wish icon
  static const IconData wish =
      IconData(0xece5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wix icon
  static const IconData wix =
      IconData(0xece6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wolfram icon
  static const IconData wolfram =
      IconData(0xece7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wolframlanguage icon
  static const IconData wolframlanguage =
      IconData(0xece8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wolframmathematica icon
  static const IconData wolframmathematica =
      IconData(0xece9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wordpress1 icon
  static const IconData wordpress1 =
      IconData(0xecea, fontFamily: _fontFamily, fontPackage: "mbi");

  /// workplace icon
  static const IconData workplace =
      IconData(0xeceb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// wpengine icon
  static const IconData wpengine =
      IconData(0xecec, fontFamily: _fontFamily, fontPackage: "mbi");

  /// write_dot_as icon
  static const IconData write_dot_as =
      IconData(0xeced, fontFamily: _fontFamily, fontPackage: "mbi");

  /// x_dot_org icon
  static const IconData x_dot_org =
      IconData(0xecee, fontFamily: _fontFamily, fontPackage: "mbi");

  /// x_pack icon
  static const IconData x_pack =
      IconData(0xecef, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xamarin icon
  static const IconData xamarin =
      IconData(0xecf0, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xbox icon
  static const IconData xbox =
      IconData(0xecf1, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xcode icon
  static const IconData xcode =
      IconData(0xecf2, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xdadevelopers icon
  static const IconData xdadevelopers =
      IconData(0xecf3, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xero icon
  static const IconData xero =
      IconData(0xecf4, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xfce icon
  static const IconData xfce =
      IconData(0xecf5, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xiaomi icon
  static const IconData xiaomi =
      IconData(0xecf6, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xing1 icon
  static const IconData xing1 =
      IconData(0xecf7, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xmpp icon
  static const IconData xmpp =
      IconData(0xecf8, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xrp icon
  static const IconData xrp =
      IconData(0xecf9, fontFamily: _fontFamily, fontPackage: "mbi");

  /// xsplit icon
  static const IconData xsplit =
      IconData(0xecfa, fontFamily: _fontFamily, fontPackage: "mbi");

  /// yahoo1 icon
  static const IconData yahoo1 =
      IconData(0xecfb, fontFamily: _fontFamily, fontPackage: "mbi");

  /// yamahacorporation icon
  static const IconData yamahacorporation =
      IconData(0xecfc, fontFamily: _fontFamily, fontPackage: "mbi");

  /// yamahamotorcorporation icon
  static const IconData yamahamotorcorporation =
      IconData(0xecfd, fontFamily: _fontFamily, fontPackage: "mbi");

  /// yammer icon
  static const IconData yammer =
      IconData(0xecfe, fontFamily: _fontFamily, fontPackage: "mbi");

  /// yandex icon
  static const IconData yandex =
      IconData(0xecff, fontFamily: _fontFamily, fontPackage: "mbi");

  /// yarn icon
  static const IconData yarn =
      IconData(0xed00, fontFamily: _fontFamily, fontPackage: "mbi");

  /// ycombinator icon
  static const IconData ycombinator =
      IconData(0xed01, fontFamily: _fontFamily, fontPackage: "mbi");

  /// yelp1 icon
  static const IconData yelp1 =
      IconData(0xed02, fontFamily: _fontFamily, fontPackage: "mbi");

  /// youtube1 icon
  static const IconData youtube1 =
      IconData(0xed03, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zalando icon
  static const IconData zalando =
      IconData(0xed04, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zapier icon
  static const IconData zapier =
      IconData(0xed05, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zeit icon
  static const IconData zeit =
      IconData(0xed06, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zend icon
  static const IconData zend =
      IconData(0xed07, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zendesk icon
  static const IconData zendesk =
      IconData(0xed08, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zendframework icon
  static const IconData zendframework =
      IconData(0xed09, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zeromq icon
  static const IconData zeromq =
      IconData(0xed0a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zerply icon
  static const IconData zerply =
      IconData(0xed0b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zhihu icon
  static const IconData zhihu =
      IconData(0xed0c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zillow icon
  static const IconData zillow =
      IconData(0xed0d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zingat icon
  static const IconData zingat =
      IconData(0xed0e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zoom icon
  static const IconData zoom =
      IconData(0xed0f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// zorin icon
  static const IconData zorin =
      IconData(0xed10, fontFamily: _fontFamily, fontPackage: "mbi");

  ///zulip icon
  static const IconData zulip =
      IconData(0xed11, fontFamily: _fontFamily, fontPackage: "mbi");
}
