import 'package:flutter/widgets.dart';

/// Creative Outline Icons
class CombiIcons {
  CombiIcons._();

  /// The icon font family name
  static const String _fontFamily = 'combi';

  /// alarm Icon
  static const IconData alarm =
      IconData(0xe900, fontFamily: _fontFamily, fontPackage: "mbi");

  /// arroba Icon
  static const IconData arroba =
      IconData(0xe901, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bar_chart Icon
  static const IconData bar_chart =
      IconData(0xe902, fontFamily: _fontFamily, fontPackage: "mbi");

  /// basketball Icon
  static const IconData basketball =
      IconData(0xe903, fontFamily: _fontFamily, fontPackage: "mbi");

  /// book Icon
  static const IconData book =
      IconData(0xe904, fontFamily: _fontFamily, fontPackage: "mbi");

  /// bug Icon
  static const IconData bug =
      IconData(0xe905, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cancel Icon
  static const IconData cancel =
      IconData(0xe906, fontFamily: _fontFamily, fontPackage: "mbi");

  /// checked Icon
  static const IconData checked =
      IconData(0xe907, fontFamily: _fontFamily, fontPackage: "mbi");

  /// checked_1 Icon
  static const IconData checked_1 =
      IconData(0xe908, fontFamily: _fontFamily, fontPackage: "mbi");

  /// circular Icon
  static const IconData circular =
      IconData(0xe909, fontFamily: _fontFamily, fontPackage: "mbi");

  /// clipboard Icon
  static const IconData clipboard =
      IconData(0xe90a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// clipboard_1 Icon
  static const IconData clipboard_1 =
      IconData(0xe90b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// clock Icon
  static const IconData clock =
      IconData(0xe90c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cloud Icon
  static const IconData cloud =
      IconData(0xe90d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cloud_computing Icon
  static const IconData cloud_computing =
      IconData(0xe90e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cloud_computing_1 Icon
  static const IconData cloud_computing_1 =
      IconData(0xe90f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cloud_computing_2 Icon
  static const IconData cloud_computing_2 =
      IconData(0xe910, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cloud_computing_3 Icon
  static const IconData cloud_computing_3 =
      IconData(0xe911, fontFamily: _fontFamily, fontPackage: "mbi");

  /// computer Icon
  static const IconData computer =
      IconData(0xe912, fontFamily: _fontFamily, fontPackage: "mbi");

  /// computer_1 Icon
  static const IconData computer_1 =
      IconData(0xe913, fontFamily: _fontFamily, fontPackage: "mbi");

  /// credit_card Icon
  static const IconData credit_card =
      IconData(0xe914, fontFamily: _fontFamily, fontPackage: "mbi");

  /// cursor Icon
  static const IconData cursor =
      IconData(0xe915, fontFamily: _fontFamily, fontPackage: "mbi");

  /// devices Icon
  static const IconData devices =
      IconData(0xe916, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dial Icon
  static const IconData dial =
      IconData(0xe917, fontFamily: _fontFamily, fontPackage: "mbi");

  /// dial_1 Icon
  static const IconData dial_1 =
      IconData(0xe918, fontFamily: _fontFamily, fontPackage: "mbi");

  /// download Icon
  static const IconData download =
      IconData(0xe919, fontFamily: _fontFamily, fontPackage: "mbi");

  /// download_1 Icon
  static const IconData download_1 =
      IconData(0xe91a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// download_2 Icon
  static const IconData download_2 =
      IconData(0xe91b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// earth_globe Icon
  static const IconData earth_globe =
      IconData(0xe91c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// edit Icon
  static const IconData edit =
      IconData(0xe91d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// edit_1 Icon
  static const IconData edit_1 =
      IconData(0xe91e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// email Icon
  static const IconData email =
      IconData(0xe91f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// email_1 Icon
  static const IconData email_1 =
      IconData(0xe920, fontFamily: _fontFamily, fontPackage: "mbi");

  /// eye Icon
  static const IconData eye =
      IconData(0xe921, fontFamily: _fontFamily, fontPackage: "mbi");

  /// flag Icon
  static const IconData flag =
      IconData(0xe922, fontFamily: _fontFamily, fontPackage: "mbi");

  /// folder Icon
  static const IconData folder =
      IconData(0xe923, fontFamily: _fontFamily, fontPackage: "mbi");

  /// glasses Icon
  static const IconData glasses =
      IconData(0xe924, fontFamily: _fontFamily, fontPackage: "mbi");

  /// head Icon
  static const IconData head =
      IconData(0xe925, fontFamily: _fontFamily, fontPackage: "mbi");

  /// head_1 Icon
  static const IconData head_1 =
      IconData(0xe926, fontFamily: _fontFamily, fontPackage: "mbi");

  /// headphones Icon
  static const IconData headphones =
      IconData(0xe927, fontFamily: _fontFamily, fontPackage: "mbi");

  /// heart Icon
  static const IconData heart =
      IconData(0xe928, fontFamily: _fontFamily, fontPackage: "mbi");

  /// hide Icon
  static const IconData hide =
      IconData(0xe929, fontFamily: _fontFamily, fontPackage: "mbi");

  /// infinity Icon
  static const IconData infinity =
      IconData(0xe92a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// laptop Icon
  static const IconData laptop =
      IconData(0xe92b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// levels Icon
  static const IconData levels =
      IconData(0xe92c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// levels_1 Icon
  static const IconData levels_1 =
      IconData(0xe92d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// light_bulb Icon
  static const IconData light_bulb =
      IconData(0xe92e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// like Icon
  static const IconData like =
      IconData(0xe92f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// list Icon
  static const IconData list =
      IconData(0xe930, fontFamily: _fontFamily, fontPackage: "mbi");

  /// login Icon
  static const IconData login =
      IconData(0xe931, fontFamily: _fontFamily, fontPackage: "mbi");

  /// login_1 Icon
  static const IconData login_1 =
      IconData(0xe932, fontFamily: _fontFamily, fontPackage: "mbi");

  /// logout Icon
  static const IconData logout =
      IconData(0xe933, fontFamily: _fontFamily, fontPackage: "mbi");

  /// logout_1 Icon
  static const IconData logout_1 =
      IconData(0xe934, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mail Icon
  static const IconData mail =
      IconData(0xe935, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mail_1 Icon
  static const IconData mail_1 =
      IconData(0xe936, fontFamily: _fontFamily, fontPackage: "mbi");

  /// megaphone Icon
  static const IconData megaphone =
      IconData(0xe937, fontFamily: _fontFamily, fontPackage: "mbi");

  /// microphone Icon
  static const IconData microphone =
      IconData(0xe938, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mouse Icon
  static const IconData mouse =
      IconData(0xe939, fontFamily: _fontFamily, fontPackage: "mbi");

  /// mouse_1 Icon
  static const IconData mouse_1 =
      IconData(0xe93a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// next Icon
  static const IconData next =
      IconData(0xe93b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// next_1 Icon
  static const IconData next_1 =
      IconData(0xe93c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// next_2 Icon
  static const IconData next_2 =
      IconData(0xe93d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// next_3 Icon
  static const IconData next_3 =
      IconData(0xe93e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// padlock Icon
  static const IconData padlock =
      IconData(0xe93f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// padlock_1 Icon
  static const IconData padlock_1 =
      IconData(0xe940, fontFamily: _fontFamily, fontPackage: "mbi");

  /// paper_plane Icon
  static const IconData paper_plane =
      IconData(0xe941, fontFamily: _fontFamily, fontPackage: "mbi");

  /// paper_plane_1 Icon
  static const IconData paper_plane_1 =
      IconData(0xe942, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pencil Icon
  static const IconData pencil =
      IconData(0xe943, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pencil_1 Icon
  static const IconData pencil_1 =
      IconData(0xe944, fontFamily: _fontFamily, fontPackage: "mbi");

  /// phone_call Icon
  static const IconData phone_call =
      IconData(0xe945, fontFamily: _fontFamily, fontPackage: "mbi");

  /// photo_camera Icon
  static const IconData photo_camera =
      IconData(0xe946, fontFamily: _fontFamily, fontPackage: "mbi");

  /// photo_camera_1 Icon
  static const IconData photo_camera_1 =
      IconData(0xe947, fontFamily: _fontFamily, fontPackage: "mbi");

  /// picture Icon
  static const IconData picture =
      IconData(0xe948, fontFamily: _fontFamily, fontPackage: "mbi");

  /// pie_chart Icon
  static const IconData pie_chart =
      IconData(0xe949, fontFamily: _fontFamily, fontPackage: "mbi");

  /// piggy_bank Icon
  static const IconData piggy_bank =
      IconData(0xe94a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// placeholder Icon
  static const IconData placeholder =
      IconData(0xe94b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// plug Icon
  static const IconData plug =
      IconData(0xe94c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// plug_1 Icon
  static const IconData plug_1 =
      IconData(0xe94d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// plug_2 Icon
  static const IconData plug_2 =
      IconData(0xe94e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// power_button Icon
  static const IconData power_button =
      IconData(0xe94f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// printer Icon
  static const IconData printer =
      IconData(0xe950, fontFamily: _fontFamily, fontPackage: "mbi");

  /// reload Icon
  static const IconData reload =
      IconData(0xe951, fontFamily: _fontFamily, fontPackage: "mbi");

  /// repeat Icon
  static const IconData repeat =
      IconData(0xe952, fontFamily: _fontFamily, fontPackage: "mbi");

  /// repeat_1 Icon
  static const IconData repeat_1 =
      IconData(0xe953, fontFamily: _fontFamily, fontPackage: "mbi");

  /// repeat_2 Icon
  static const IconData repeat_2 =
      IconData(0xe954, fontFamily: _fontFamily, fontPackage: "mbi");

  /// reply Icon
  static const IconData reply =
      IconData(0xe955, fontFamily: _fontFamily, fontPackage: "mbi");

  /// screen Icon
  static const IconData screen =
      IconData(0xe956, fontFamily: _fontFamily, fontPackage: "mbi");

  /// screen_1 Icon
  static const IconData screen_1 =
      IconData(0xe957, fontFamily: _fontFamily, fontPackage: "mbi");

  /// search Icon
  static const IconData search =
      IconData(0xe958, fontFamily: _fontFamily, fontPackage: "mbi");

  /// server Icon
  static const IconData server =
      IconData(0xe959, fontFamily: _fontFamily, fontPackage: "mbi");

  /// settings Icon
  static const IconData settings =
      IconData(0xe95a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// settings_1 Icon
  static const IconData settings_1 =
      IconData(0xe95b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// settings_2 Icon
  static const IconData settings_2 =
      IconData(0xe95c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// share Icon
  static const IconData share =
      IconData(0xe95d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shopping_bag Icon
  static const IconData shopping_bag =
      IconData(0xe95e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shuffle Icon
  static const IconData shuffle =
      IconData(0xe95f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shuffle_1 Icon
  static const IconData shuffle_1 =
      IconData(0xe960, fontFamily: _fontFamily, fontPackage: "mbi");

  /// shuffle_2 Icon
  static const IconData shuffle_2 =
      IconData(0xe961, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sitemap Icon
  static const IconData sitemap =
      IconData(0xe962, fontFamily: _fontFamily, fontPackage: "mbi");

  /// sledge Icon
  static const IconData sledge =
      IconData(0xe963, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speaker Icon
  static const IconData speaker =
      IconData(0xe964, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speaker_1 Icon
  static const IconData speaker_1 =
      IconData(0xe965, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble Icon
  static const IconData speech_bubble =
      IconData(0xe966, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble_1 Icon
  static const IconData speech_bubble_1 =
      IconData(0xe967, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble_2 Icon
  static const IconData speech_bubble_2 =
      IconData(0xe968, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble_3 Icon
  static const IconData speech_bubble_3 =
      IconData(0xe969, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble_4 Icon
  static const IconData speech_bubble_4 =
      IconData(0xe96a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// speech_bubble_5 Icon
  static const IconData speech_bubble_5 =
      IconData(0xe96b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// switch_1 Icon
  static const IconData switch_1 =
      IconData(0xe96c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// switch_2 Icon
  static const IconData switch_2 =
      IconData(0xe96d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// tag Icon
  static const IconData tag =
      IconData(0xe96e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// target Icon
  static const IconData target =
      IconData(0xe96f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// telephone Icon
  static const IconData telephone =
      IconData(0xe970, fontFamily: _fontFamily, fontPackage: "mbi");

  /// transfer Icon
  static const IconData transfer =
      IconData(0xe971, fontFamily: _fontFamily, fontPackage: "mbi");

  /// transfer_1 Icon
  static const IconData transfer_1 =
      IconData(0xe972, fontFamily: _fontFamily, fontPackage: "mbi");

  /// trash Icon
  static const IconData trash =
      IconData(0xe973, fontFamily: _fontFamily, fontPackage: "mbi");

  /// trash_1 Icon
  static const IconData trash_1 =
      IconData(0xe974, fontFamily: _fontFamily, fontPackage: "mbi");

  /// umbrella Icon
  static const IconData umbrella =
      IconData(0xe975, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upload Icon
  static const IconData upload =
      IconData(0xe976, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upload_1 Icon
  static const IconData upload_1 =
      IconData(0xe977, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upload_2 Icon
  static const IconData upload_2 =
      IconData(0xe978, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upload_3 Icon
  static const IconData upload_3 =
      IconData(0xe979, fontFamily: _fontFamily, fontPackage: "mbi");

  /// upload_4 Icon
  static const IconData upload_4 =
      IconData(0xe97a, fontFamily: _fontFamily, fontPackage: "mbi");

  /// user Icon
  static const IconData user =
      IconData(0xe97b, fontFamily: _fontFamily, fontPackage: "mbi");

  /// user_1 Icon
  static const IconData user_1 =
      IconData(0xe97c, fontFamily: _fontFamily, fontPackage: "mbi");

  /// user_2 Icon
  static const IconData user_2 =
      IconData(0xe97d, fontFamily: _fontFamily, fontPackage: "mbi");

  /// user_3 Icon
  static const IconData user_3 =
      IconData(0xe97e, fontFamily: _fontFamily, fontPackage: "mbi");

  /// users Icon
  static const IconData users =
      IconData(0xe97f, fontFamily: _fontFamily, fontPackage: "mbi");

  /// volume Icon
  static const IconData volume =
      IconData(0xe980, fontFamily: _fontFamily, fontPackage: "mbi");
}
