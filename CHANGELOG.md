## [0.0.8]

* HandmadeIcons were added.
## [0.0.7]

* Fix some issues.
## [0.0.5]

* EmojiIcons and CombiIcons were added.
## [0.0.4]

* BrandsIcons and FileIcons were added.
## [0.0.2]

* Created usage example

## [0.0.0] - 8/3/2020

* Initial release
