import 'package:flutter/material.dart';
import 'package:mbi/mbi.dart';

main() {
  runApp(MainApp());
}

class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: IconButton(
            // Use the FeatherIcons class for the IconData
            icon: Icon(BrandsIcons.apache),
            onPressed: () {
              print("Pressed");
            }),
      ),
    );
  }
}
