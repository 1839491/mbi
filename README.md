# mbi

The [MBI](https://mbcodes.ir/) Icon pack created by [MBCodes](https://gitlab.com/1839491/mbi) available as set of Flutter Icons.

## Installation

In the `dependencies:` section of your `pubspec.yaml`, add the following line:

```yaml
dependencies:
  mbi: ^0.0.8
```

## Usage

```dart
import 'package:mbi/mbi.dart';

class MyWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return new IconButton(
      icon: new Icon(BrandsIcons.apache), 
      onPressed: () { print("Pressed"); }
     );
  }
}
```

## Contributors

  - SattarKhan
  - BagherKhan
  
# Icons
#### FileIcons
   ![FileIcons](https://dl2.apexteam.net/mym/fileicons-mbi.png)
#### LineconsIcons
   ![LineconsIcons](https://dl2.apexteam.net/mym/linecons.png)
#### BrandsIcons
   ![FileIcons](https://dl2.apexteam.net/mym/brands-icons-mbi.png)
#### CombiIcons
   ![CombiIcons](https://dl2.apexteam.net/mym/combi-icons-list.png)
#### EmojiIcons
   ![HandmadeIcons](https://dl2.apexteam.net/mym/emoji-mbi-icons.png)
#### EmojiIcons
   ![HandmadeIcons](https://dl2.apexteam.net/mym/Handmade-mbi-icons.png)